        section .data
rfmt:   db "result: %x" , 0xa, 0

sample: db 0x07
         
        segment .text
        global  main
        extern printf


main:   
        
        
        
        neg byte [sample]  ; 

        



        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        
        lea     rdi, [rfmt]  ; parameter 1 for printf
        lea     rsi, [sample]; parameter 2 for printf
        and     rsi, 0xFF
        xor     eax, eax     ; 0 floating point parameters
        call    printf


         mov  eax,1       ; 1 is the exit syscall number
         mov  ebx,0       ; the status value to return
         int 0x80
