        section .data
rfmt:   db "result: %d" , 0xa, 0

         
        segment .text
        global  main
        extern printf


main:   

        xor rbx, rbx ; clear out rbx
        xor rsi, rsi ; clear out rsi
        


        mov rbx, 200  ; loading 200
        neg rbx       ; expect -200





        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rbx     ; parameter 2 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


         mov  eax,1       ; 1 is the exit syscall number
         mov  ebx,0       ; the status value to return
         int 0x80
