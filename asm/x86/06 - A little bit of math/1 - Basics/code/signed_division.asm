        section .data
rfmt:   db "result: %d = %d / %d" , 0xa, 0


a:      dq -120
b:      dq 2

         
        segment .text
        global  main
        extern printf


main:   
        xor rax, rax ; clear out rbx and use it as the accumulator
        xor rdx, rdx ; clear out rbx and use it as the accumulator
        
        
        
                     ; rax = 0
        mov rax, [a] ; rax = a 
        
        idiv qword [b] ; rdx:rax = a * b  ; we're assuming that we didn't overflow






        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rax     ; parameter 2 for printf
        mov     rdx, [a]     ; parameter 3 for printf
        mov     rcx, [b]     ; parameter 4 for printf
        xor     eax, eax     ; 0 floating point parameters
        call    printf


         mov  eax,1       ; 1 is the exit syscall number
         mov  ebx,0       ; the status value to return
         int 0x80
