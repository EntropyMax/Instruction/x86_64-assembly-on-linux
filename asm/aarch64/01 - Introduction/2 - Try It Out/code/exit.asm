
.text
.globl  _start

_start:
    mov x8, 0x5D // 5d is the exit syscall number (don't worry we'll get to that later)
    mov x0, 5    // the status value to return
    svc 0        // execute a system call

