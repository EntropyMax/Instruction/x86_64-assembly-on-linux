        section .data
rfmt:   db "Seeked to this line:", 0xa, "%s     which ended at position %ld" , 0xa, 0
ffmt:   db "opening this file %s in mode %s", 0xa, 0



        segment .text
        global  main
        extern printf, fopen, fclose, fseek, fgets, ftell, calloc, free


main:   
        mov r14, rdi        ; move argc into my register
        mov r15, rsi        ; move argv into my register
        xor rbx, rbx        ; prep the counter
        
        cmp r14, 3
        jne done            ; expect to have a parameter for the filename
        
        
        add r15, 8          ; first parameter is posix defined as the program name, don't care, go to next
        
        mov r14, r15
        add r14, 8          ; file open mode should now be in r14
        
        
        lea     rdi, [ffmt]         ; parameter 1 for printf
        mov     rsi, [r15]          ; parameter 2 for printf - the string
        mov     rdx, [r14]          ; parameter 3 for printf - the mode
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        
        
        ; FILE *fopen(char *pathname, char *mode)
        mov     rdi, [r15]    ; parameter 1
        mov     rsi, [r14]    ; parameter 2
        xor     rax, rax      ; 0 floating point parameters
        call    fopen
        mov r13, rax          ;r13 is my file handle
        
        
        
        
        ; fseek(FILE *stream, int32_t offset, int32_t wence);
        mov     rdi, r13      ; parameter 1 stream
        mov     rsi, 28992    ; parameter 2 offset 
        mov     rdx, 0        ; parameter 3 0
        xor     rax, rax      ; 0 floating point parameters
        call    fseek
        
        
        
        ; allocate some space for the line RFC max line length is 80, lets do 160 to be double "safe"
        mov     rdi, 1      ; parameter 1 objects
        mov     rsi, 160    ; parameter 2 obj size
        xor     rax, rax    ; 0 floating point parameters
        call    calloc
        mov     r12, rax    ; pointer to my memory
        



        ;char *fgets(char *str, int n, FILE *stream)
        mov     rdi, r12    ; parameter 1 bunch of memory
        mov     rsi, 160    ; parameter 2 max size of my memory
        mov     rdx, r13    ; parameter 3 my file handle
        xor     rax, rax    ; 0 floating point parameters
        call    fgets
        
        
        
        ; int ftell(FILE *stream);
        mov     rdi, r13    ; parameter 1 my file handle
        xor     rax, rax    ; 0 floating point parameters
        call    ftell
        mov     r11, rax    ;  r11 has the location my line ends
        
        
        
        
        lea     rdi, [rfmt]         ; parameter 1 for printf
        mov     rsi, r12            ; parameter 2 for printf - the string
        mov     rdx, r11            ; parameter 3 for printf - file position
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        
        
        
        mov     rdi, r12    ; parameter 1 my mem
        xor     rax, rax    ; 0 floating point parameters
        call    free
        
        mov     rdi, r13    ; parameter 1 my file handle
        xor     rax, rax    ; 0 floating point parameters
        call    fclose
        
        
        
        
        
        
        
done:        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call