        section .data
rfmt:   db "result: %x" , 0xa, 0

         
        segment .text
        global  main
        extern printf


main:   

        xor rbx, rbx ; clear out rbx
        xor rsi, rsi ; clear out rsi
        


        mov rbx, 0xDB  ; loading db
        and rbx, 0x0F  ; expect 0x0B





        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        and rbx, 0xFF        ; cleaning up the result for the print
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rbx     ; parameter 2 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; 0x3C is the exit syscall number
        mov rdi,0     ; the status value to return
        syscall       ; execute a system call
