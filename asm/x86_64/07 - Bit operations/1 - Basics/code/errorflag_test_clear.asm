        section .data
rfmt:   db "saw an error fla" , 0xa, 0

         
        segment .text
        global  main
        extern printf

someone_elses_work: 
        and r15, 0xFD      ; other person knew r15 is the source register
                           ; other person handles the error simply by removing the flag ... ok
        jmp close_program  ; other person is responsible to close down the program 
                       

main:   

        xor rbx, rbx ; clear out rbx
        xor rsi, rsi ; clear out rsi
        


        mov  rbx, 0xDB   ;  Initial Source data
        mov  r15, rbx    ;  make a backup of the source data
        
        shr  r15, 2      ;  
        and  r15, 0x01   ; 

        cmp rbx, 1              ; comparing if the error flag is set
        je  someone_elses_work  ; if the flag was set, go to their work
                                ; if the flag was NOT set, allow program to close


close_program:
        mov rax, 0x3C ; 0x3C is the exit syscall number
        mov rdi,0     ; the status value to return
        syscall       ; execute a system call
