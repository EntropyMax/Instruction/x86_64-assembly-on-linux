{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='  padding: 10px; border-style: solid; background: #336600;  color: white;  background-size: 100% 100%; background-repeat: no-repeat;'>\n",
    "  <div style='  padding: 25px; text-align: center; margin: 20px; border-style: double; font-size: 30px;'>\n",
    "      <h1 style='text-shadow: -2px -2px 0 #000, 2px -2px 0 #000, -2px 2px 0 #000, 2px 2px 0 #000;'>\n",
    "          15.1.3 Binary Trees\n",
    "      </h1>\n",
    "      \n",
    "  </div>\n",
    "  <div style=' color: #EEEEEE; text-align: left; font-size: 20px;'>\n",
    "      <h2>\n",
    "          Goals:\n",
    "      </h2>\n",
    "      <ol>\n",
    "          <li>Understand the concept of a Binary Tree</li>\n",
    "          <li>Understand the concept of a balanced vs unbalanced Binary Tree</li>\n",
    "          <li>Understand the balancing is performed by modifying the insertion order</li>\n",
    "          <li>Understand the concept of how a Binary Tree is generalized by a B-Tree</li>\n",
    "          <li>Understand the how to implement a Binary Tree in assembly</li>\n",
    "      </ol>\n",
    "  </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Refresher on Binary Trees\n",
    "\n",
    "A Binary Tree is a ***managed indexing*** scheme for data.  It is ***managed*** because the choice of how and when to insert data into the tree yields the results on performance.  It is an ***indexing*** scheme because it uses the properties of a linked list, but <span style=\"color:blue;\">seeks to reduce the average number of hops through the list</span>.  An unbalanced Binary Tree acts the same as a linked list.  \n",
    "\n",
    "- An unbalanced binary tree (or Linked List) behaves as O(N)\n",
    "- A well balanced Binary Tree behaves as O(log<sub>2</sub>N)\n",
    "\n",
    "### Binary Tree vs Hash Table\n",
    "\n",
    "A Binary tree comes in handy when you would like to observe partial or incomplete matches, still using an index.  A Hash Table computes some hash digest of the input before accessing the Hash Table and retreiving the output.  Therefore a Hash Table has no ability to find close or partial matches.  For instance, finding all usernames which start with `Da` could not be performed using a Hash Table, but could be performed using a Binary Tree to access names `Dan`, `Daisy`, `Dashea`, or `Dakota`.  Ultimately, the choice of data structure depends on what you intend to do with the data in question.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Binary Tree Object\n",
    "\n",
    "A binary Tree Object usually consists of at least 4 elements in the struct.  \n",
    "1. An *index* key which can execute <span style=\"color:green;\">less-than</span> or <span style=\"color:red;\">greater-than</span> operations\n",
    "2. A *left* pointer to another Binary Tree object whose **index** value is <span style=\"color:green;\">less-than</span> its own (may be a NULL pointer).\n",
    "3. A *right* pointer to another Binary Tree object whose **index** value is <span style=\"color:red;\">greater-than</span> its own (may be a NULL pointer).\n",
    "4. A optional expanded data set represented by the index value\n",
    "\n",
    "\n",
    "Visually, it may help to think of a binary tree struct that looks like this:\n",
    "\n",
    "\n",
    "| Binary Tree Object |\n",
    "|:-|\n",
    "|![data1](gv/binary_tree_object.gv.png)|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### C Struct implementation \n",
    "In C, we might build a struct that looks like the following using an integer as the key:\n",
    "\n",
    "``` c\n",
    "    struct bt_item {\n",
    "        int64_t         key;             //  8 bytes  (qw)\n",
    "        struct bt_item *left;            //  8 bytes  (qw)\n",
    "        struct bt_item *right;           //  8 bytes  (qw)\n",
    "        void           *data;            //  8 bytes  (qw)\n",
    "    };\n",
    "```\n",
    "\n",
    "Note that this example uses a void pointer to the data, because the Binary Tree representation doesn't care about that field and will never inspect it, but is willing to hold a pointer to some user generated content. \n",
    "\n",
    "\n",
    "\n",
    "### Creation / Inserting items into the Binary Tree\n",
    "\n",
    "The order in which you insert items into the Binary Tree could have dramatic effect on the performance of the tree. \n",
    "\n",
    "Assume you have the following items you'd like to represent in a binary tree.\n",
    "\n",
    "| Representative items for use in a Binary Tree |\n",
    "|:-|\n",
    "|![data](gv/binary_tree_items.gv.png)|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Naive insert from left to right.  \n",
    "\n",
    "1. Place 100 as the root note\n",
    "2. 12 is less than 100, so place in the left of 100\n",
    "3. 77 is less than 100, then greater than 12, so place in the right of 12.\n",
    "4. 50 is less than 100, then greater than 12, then less than 77, so place in the left of 77\n",
    "5. 200 is greater than 100, so place in the right of 100\n",
    "6. 75 is less than 100, then greater than 12, then less than 77, then greater than 50, so place to the right of 50\n",
    "\n",
    "Visually, the unmanaged (left to right) inserts would result in a tree that looks like this:\n",
    "\n",
    "Notice that we start well at the top with a good split, but the left branch of the tree begins to look like a linked list.\n",
    "\n",
    "| Inserting Left to Right |\n",
    "|:-|\n",
    "|![data](gv/binary_tree_LtoR_inserts.gv.png)|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Benefits of an unbalanced Binary Tree\n",
    "\n",
    "We still maintain strong benefits of an Unbalanced Binary Tree over a Linked list.\n",
    "\n",
    "By inspecting only 2 elements, we know there is no data in this tree less than 12, or greater than 200.  For this reason, Binary Trees *could* be useful in cases where bounds checking is desired.  \n",
    "\n",
    "\n",
    "#### Drawbacks of an unbalanced Binary Tree\n",
    "\n",
    "Notice that in the unfortunate case where the user wishes to access the item using key `75`, the performance is not much better tha a Linked List.  When the Binary Tree contains hundreds, thousands, millions, or more items in the tree, this descent through the branches of an unbalanced tree may negate the anticipated benefits of the Binary Tree."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Precomputing optimized layout\n",
    "\n",
    "In some cases, it may be worth precomputing the insertion arrangement such that the tree comes relatively <span style=\"color:blue;\">well balanced</span> and has <span style=\"color:blue;\">large gaps for future inserts</span>\n",
    "\n",
    "Precomputing would inspect the entire dataset and return an insertion ordering such that the resulting tree would be well balanced leaving large gaps.  In this example, assume a function that consumes the Left to Right order and emits an insertion order such as `75, 100, 200, 77, 50, 12`.  The hypothetical function could also emit an ordering such as `77, 50, 75, 12, 100, 200`\n",
    "\n",
    "This hypothetical function can be as complicated as required, but generally looks to accomplish two objctives\n",
    "1. <span style=\"color:blue;\">well balanced</span> - the tree should should be short (least hops to reach a leaf)\n",
    "2. <span style=\"color:blue;\">large gaps for future inserts</span> - The key to the left/right of the current should be as distant as possible \n",
    "\n",
    "\n",
    "| Precomputed inserts  75, 100, 200, 77, 50, 12 | Precomputed inserts  77, 50, 75, 12, 100, 200 | \n",
    "|:-|-:|\n",
    "|![data](gv/binary_tree_precomputed_inserts.gv.png)| ![data](gv/binary_tree_precomputed_inserts2.gv.png) |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Understanding which option is more optimal\n",
    "\n",
    "Given the two criteria above, we can see that both options has a depth of 3 (<span style=\"color:blue;\">well balanced</span> achieved) but the ordering is substantially different between the two options.  We can compute the <span style=\"color:blue;\">large gaps for future inserts</span> by calculating the sum of the gaps between values.  From the calculation below, we see option 1 is slightly *better* in that it provides bigger gaps for future inserts.\n",
    "\n",
    "Ultimately, a data index may have different optimiaztion critera that can append other considerations to this list.  In all cases, it is probable to require a well balanced tree with large gaps, but may additionally require other considerations such as inspection into the opaque *(data)* to place commonly accessed data near the root of the tree.  Only the programer with understanding of the problem at hand will be able to dictate additional optimiaztion criteria."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "75,100,200,77,50,12 order gap total = 313\n",
      "77,50,75,12,100,200 order gap total = 303\n"
     ]
    }
   ],
   "source": [
    "distance_option_1 = abs(75-100) + abs(100-200) + abs(200-77) + abs(77-50) + abs(50-12)\n",
    "distance_option_2 = abs(77-50) + abs(50-75) + abs(75-12) + abs(12-100) + abs(100-200)\n",
    "print(f\"75,100,200,77,50,12 order gap total = {distance_option_1}\\n77,50,75,12,100,200 order gap total = {distance_option_2}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Benefits of an optimized Binary Tree\n",
    "\n",
    "<span style=\"color:blue;\">well balanced</span> trees allow for read operations to perform closer to the optimal performance - O(log<sub>2</sub>N) \n",
    "\n",
    "<span style=\"color:blue;\">large gaps for future inserts</span> allow for write operations to perfom closer to optimal performance because keys are able to find available gaps to fit within without expanding the depth of the tree.  \n",
    "\n",
    "#### Drawbacks of an optimized Binary Tree\n",
    "\n",
    "Pre-computing optimal insertion order may be non-trivial. For hundreds, thousands, or millions of items, it may become exceedingly difficult to find great, let alone optimal insertion ordering.  In these cases, *good enough* may still be better than the Naive inserts, but still distant from optimal.  \n",
    "\n",
    "For read-heavy datasets, it may be well worth the effort to spend time computing the optimal insertion order.  For write-heavy datasets such as streaming logs, precomputing the optimal order may provide immediate gain, but future writes (especially writes containing unknown keys) may quickly de-optimize the tree, requiring a constant arms race of index rebuilds as new data is inserted.  The programmer must then compare the CPU/Memory/Time constrants on constantly rebuilding the index versus the detriment to read-access.  On exceedingly write heavy data (log streaming) with keys considered random, it may be better to let the randomness of the data balance the tree on its own.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binary Tree generalized to a B-Tree\n",
    "\n",
    "Where the Binary Tree has two options (<span style=\"color:green;\">left</span> and  <span style=\"color:red;\">right</span>) that are usually implemented with <span style=\"color:green;\">less-than</span> or <span style=\"color:red;\">greater-than</span> operations, there is no requirement that those operations be used.  Any operation that can seperate a branch left or branch right is acceptable.\n",
    "\n",
    "We can generalize a Binary Tree into a B-Tree by including operations that have more than two distinct states.  For instance, we can include 4 transition states (<span style=\"color:MediumSeaGreen;\">very left</span>, <span style=\"color:green;\">left</span>, <span style=\"color:red;\">right</span>, <span style=\"color:darkred;\">very right</span>) using custom operations.  We could also invent asymetrical B-Trees or anything else such that we also invent a way to calculate which branch to take.\n",
    "\n",
    "We can arbitrarily invent a notion that \n",
    "- <span style=\"color:MediumSeaGreen;\">Very Left</span>  - more than 25 less than current value\n",
    "- <span style=\"color:green;\">Left</span>       - 1-25 less than the current value\n",
    "- <span style=\"color:red;\">Right</span>      - 1-25 greater than the current value\n",
    "- <span style=\"color:darkred;\">Very Right</span> - more than 25 greater than the current value \n",
    "\n",
    "\n",
    "\n",
    "``` c\n",
    "    struct bt_item {\n",
    "        int64_t         key;             //  8 bytes  (qw)\n",
    "        struct bt_item *very_left;       //  8 bytes  (qw)\n",
    "        struct bt_item *left;            //  8 bytes  (qw)\n",
    "        struct bt_item *right;           //  8 bytes  (qw)\n",
    "        struct bt_item *very_right;      //  8 bytes  (qw)\n",
    "        void           *data;            //  8 bytes  (qw)\n",
    "    };\n",
    "```\n",
    "\n",
    "| B-Tree Four-State Generalization |\n",
    "|:-|\n",
    "|![data](gv/b_tree_object_4states.gv.png)|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Insertion order still matters\n",
    "\n",
    "The same constraints on insertion order (<span style=\"color:blue;\">well balanced</span> and has <span style=\"color:blue;\">large gaps for future inserts</span>) still exist with B-Trees.  Here are two examples of different insertion ordering.\n",
    "\n",
    "| B-Tree Four-State Insertion 77,200,100,75,50,12 |\n",
    "|:-|\n",
    "|![data](gv/binary_tree_precomputed_inserts_4state_1.gv.png)|\n",
    "\n",
    "| B-Tree Four-State Generalization 77,200,100,75,12,50 |\n",
    "|:-|\n",
    "|![data](gv/binary_tree_precomputed_inserts_4state_2.gv.png)|\n",
    "\n",
    "\n",
    "#### B-Tree Benefits\n",
    "\n",
    "Even with the **worst possible insertion order for a Binary Tree (sorted inserts)** resulting in a long right linked-list, a B-Tree can provide some value.\n",
    "\n",
    "|Binary Tree Sorted Inserts 12,40,75,77,100,200 | B-Tree Sorted Inserts 12,40,75,77,100,200 |\n",
    "|:-|-:|\n",
    "|![data](gv/binary_tree_precomputed_inserts_worst.gv.png)|![data](gv/binary_tree_precomputed_inserts_4state_3.gv.png)|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementing a Binary Tree in Assembly\n",
    "\n",
    "We start with a general purpose structure with a 8 byte key, left/right pointers, and a data pointer.\n",
    "\n",
    "In this case, we'll put an integer in the key value, but it could be anything as long as we have a way to calculate the branch right or branch left based on our choice. \n",
    "\n",
    "\n",
    "``` nasm\n",
    "         struc      bt_item \n",
    "b_key    resq       1\n",
    "b_left   resq       1\n",
    "b_right  resq       1\n",
    "b_data   resq       1\n",
    "         align   8\n",
    "         endstruc\n",
    "```\n",
    "\n",
    "### Programming Problem\n",
    "\n",
    "We will read the contents of the command line parameters ([Reference Chapter 10, Module 5](../../../../../notebooks/asm/x86_64/10%20-%20Arrays/1%20-%20Basics/5%20-%20Command%20Line%20Parameter%20Array.ipynb)).  Each parameter should be a string representation of a numerical value.  \n",
    "\n",
    "Our program will create a binary tree based on the numerical value of the arguments passed in.  \n",
    "\n",
    "## Compile, Link, Execute\n",
    "\n",
    "The [full source of this example is located here](../../../../../edit/asm/x86_64/15%20-%20Data%20Structures/1%20-%20Basics/code/binary_tree.asm)\n",
    "\n",
    "The printing functions `_dbg_*`that make up the bulk of the source can generally be ignored.  They simply print the behavior to stdout `_dbg_cmp_left`, `_dbg_cmp_right`, `_dbg_place_right`, `_dbg_place_left`, `_dbg_descend_right`, `_dbg_descend_left`.\n",
    "\n",
    "\n",
    "The `allocate_btitem` allocates exactly one binary tree item onto the heap using calloc (zeros out the struct) and sets the key to new_val.\n",
    "``` c\n",
    "struct bt_item * allocate_btitem(int64_t new_val);\n",
    "```\n",
    "\n",
    "The `place_btitem` is a potentially recursive function that take a presumed root object and figures out where to put this new leaf node in the Binary Tree.  It will recursively call itself as it branches down the tree\n",
    "\n",
    "``` c \n",
    "void place_btitem(struct bt_item * root, int64_t new_val);\n",
    "```\n",
    "\n",
    "The main function begins by parsing the program arguments.  The first argument `argv[0]` is always the name of the program, so we'll skip that one.  The second argument `argv[1]` we'll use as our root node.  Remember from above, by the time we're generating a binary tree, the tree itself is ignorant of the data we're about to give it.  We learned that this particular order (`75, 100, 200, 77, 50, 12`) is a good  choice, but the Binary Tree will build itself whether the choice is good or bad.  \n",
    "\n",
    "Once the root node is established, the program begins its loop through the rest of the application arguments.\n",
    "\n",
    "It may be a good exercise to whiteboard the flow through this program.  You should feel comfortable discussing the control flow observed watching stdout for credit with this module.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! yasm \\\n",
    "    -f elf64 \\\n",
    "    -o code/binary_tree.o \\\n",
    "    code/binary_tree.asm && echo \"The code assembled successfully, continue to the next step\" \n",
    "\n",
    "\n",
    "! ls -alh code/binary_tree.o"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! gcc \\\n",
    "    -no-pie \\\n",
    "    -o code/binary_tree \\\n",
    "    code/binary_tree.o\n",
    "\n",
    "! ls -alh code/binary_tree*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! code/binary_tree 75 100 200 77 50 12"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! rm -rf code/binary_tree.o code/binary_tree\n",
    "! ls -alh code/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
