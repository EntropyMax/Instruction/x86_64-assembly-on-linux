;%cflags: -f elf64
;%ldflags: -no-pie
;%args: 75 100 200 77 50 12

; edit the args field above to add more or rearrange to observe how the Binary Tree fills

        section .data
rfmt:   db "arg[%d] = %s" , 0xa, 0
bgn:    db "%lld becomes root node of the tree",0xa,0 
cmpr:   db "%lld compares to the right of %lld", 0xa, 0
cmpl:   db "%lld compares to the left of %lld", 0xa, 0
jmpr:   db "%lld jumps right of %lld", 0xa, 0
jmpl:   db "%lld jumps left of %lld", 0xa, 0
fndr:   db "%lld placed to the right of %lld", 0xa, 0
fndl:   db "%lld placed to the left of %lld", 0xa, 0

         struc      bt_item 
b_key    resq       1
b_left   resq       1
b_right  resq       1
b_data   resq       1
         align   8
         endstruc


        segment .text
        global  main
        extern printf, calloc, free, strdup, atoi


_dbg_cmp_left:
        push rdi
        push rsi
        push rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [cmpl]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop rdx
        pop rsi
        pop rdi
        ret
        
_dbg_cmp_right:
        push    rdi
        push    rsi
        push    rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [cmpr]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop     rdx
        pop     rsi
        pop     rdi
        ret
        
_dbg_place_right:
        push    rdi
        push    rsi
        push    rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [fndr]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop     rdx
        pop     rsi
        pop     rdi
        ret  
        
_dbg_place_left:
        push    rdi
        push    rsi
        push    rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [fndl]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop     rdx
        pop     rsi
        pop     rdi
        ret  
        
_dbg_descend_right:
        push    rdi
        push    rsi
        push    rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [jmpr]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop     rdx
        pop     rsi
        pop     rdi
        ret   
        
_dbg_descend_left:
        push    rdi
        push    rsi
        push    rdx
        mov     rdx, [rdi+b_key]
        lea     rdi, [jmpl]         ; parameter 1 for printf
                                    ; rsi already populated
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        pop     rdx
        pop     rsi
        pop     rdi
        ret 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; struct bt_item * allocate_btitem(int64_t new_val);
allocate_bt_item:  
        push rbp                ; push the base pointer to onto the stack
        mov  rbp, rsp           ; move the stack pointer into the base pointer
        sub  rsp, 32            ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        mov  [rsp], rdi         ; holding new_val
        mov  rdi, 1             ; automatically generated label from the compiler
        mov  rsi, bt_item_size  ; one bt_item somewhere
        call calloc             ; reminder: using calloc to null out the memory so we can branch on nulls
        mov  rsi, [rsp]
        mov  [rax+b_key], rsi
        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;  potentially recursive
; 
; void place_btitem(struct bt_item * root, int64_t new_val);
place_btitem:
        push    rbp                ; push the base pointer to onto the stack
        mov     rbp, rsp           ; move the stack pointer into the base pointer
        sub     rsp, 32            ; space from the stack pointer to reserve
        mov     [rsp], rdi         ; hold a backup to rdi on the stack since the debug fn blows it away
        cmp     rsi, [rdi+b_key]   ; compare bt_item.b_key
        jg      .checkright   
.checkleft:
        call    _dbg_cmp_left          ; dirty, not following calling convention
        mov     rcx, [rdi+b_left] 
        cmp     rcx, 0
        jne     .descendleft
.placeleft:
        call    _dbg_place_left
        mov     rdi, rsi
        call    allocate_bt_item
        mov     rdi, [rsp]
        mov     [rdi+b_left], rax      ; set right to the new memory address
        jmp     .doneplacing
.descendleft:
        call    _dbg_descend_left
        mov     rdi, [rdi+b_left]
        call    place_btitem
        jmp     .doneplacing
.checkright:
        call    _dbg_cmp_right          ; dirty, not following calling convention
        mov     rcx, [rdi+b_right] 
        cmp     rcx, 0
        jne     .descendright
.placeright
        call    _dbg_place_right
        mov     rdi, rsi
        call    allocate_bt_item
        mov     rdi, [rsp]
        mov     [rdi+b_right], rax      ; set right to the new memory address
        jmp     .doneplacing
.descendright
        call    _dbg_descend_right
        mov     rdi, [rdi+b_right]
        call    place_btitem
.doneplacing:
        leave
        ret
;
; end func place_btitem
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; int main (argc, argv);
main:   
        mov r14, rdi                ; move argc into my register
        mov r15, rsi                ; move argv into my register
        xor r13, r13                ; prep the counter
        add     r15, 8              ; arg 0 is the program name 
        
        
        call allocate_bt_item       ; allocating with some random value for now
        mov r12, rax                ; r12 will hold the root of the tree
        mov     rdi, [r15]
        call    atoi
        mov     [r12+b_key], rax    ; setting the root node value
        
        ;;;;;;;; Output ;;;;;;;;  
        lea     rdi, [bgn]          ; parameter 1 for printf
        mov     rsi, [r12+b_key]    ; parameter 2 for printf
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        ;;;;;;;; /Output ;;;;;;;;
        
        add     r15, 8
        add     r13, 2
        
has_more:
        mov     rdi, [r15]
        call    atoi
        mov     rdi, r12             
        mov     rsi, rax            ; rdi is a number now
        call    place_btitem
        

        inc     r13
        add     r15, 8              ;  we know that the next pointer is 8 bytes away
        cmp     r13, r14
        jne     has_more

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

done:        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
