        section .data
rfmt:   db "%s " , 0xa, 0

hello:  db "Hello", 0
linked: db "Linked", 0
world:  db "World", 0



         struc      ll_item 
.p       resq       1
.next    resq       1
         align   8
         endstruc


        segment .text
        global  main
        extern printf, calloc, free, strdup

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
main:   
allocate_first:     
        mov  rdi, ll_item_size ; automatically generated label from the compiler
        mov  rsi, 1       ; one ll_item somewhere
        call calloc       ; reminder: using calloc to null out the memory so we can branch on nulls
        mov r15, rax      ; hold onto the head pointer in r15
        
        mov r14, rax      ; holding onto a 
        lea  rdi, [hello]         ; automatically generated label from the compiler
        call strdup               ; more stuff randomly placed on heap
        mov [r14+ll_item.p], rax  ; put the pointer to the heap allocated string in the struc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
allocate_second:     
        mov  rdi, ll_item_size       ; automatically generated label from the compiler
        mov  rsi, 1                  ; one ll_item somewhere
        call calloc                  ; reminder: using calloc to null out the memory so we can branch on nulls

        mov [r14+ll_item.next], rax  ; set the pointer to this new guy first
        mov r14, rax
        
        lea  rdi, [linked]             ; automatically generated label from the compiler
        call strdup               ; more stuff randomly placed on heap
        mov [r14+ll_item.p], rax  ; put the pointer to the heap allocated string in the struc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
allocate_third:     
        mov  rdi, ll_item_size       ; automatically generated label from the compiler
        mov  rsi, 1                  ; one ll_item somewhere
        call calloc                  ; reminder: using calloc to null out the memory so we can branch on nulls

        mov [r14+ll_item.next], rax  ; set the pointer to this new guy first
        mov r14, rax
        
        lea  rdi, [world]             ; automatically generated label from the compiler
        call strdup               ; more stuff randomly placed on heap
        mov [r14+ll_item.p], rax  ; put the pointer to the heap allocated string in the struc


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        mov     r14, r15     ; start at the head        
looped_printing 
        lea     rdi,  [rfmt]           ; parameter 1 for printf
        mov     rsi,  [r14+ll_item.p]  ; parameter 2 for printf
        xor     rax, rax               ; 0 floating point parameters
        call    printf 
        mov     r14, [r14+ll_item.next]
        cmp     r14, 0                 ; our walking pointer is null, terminate loop / don't jump back
        jnz     looped_printing        ; keep looping if it's not a null
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

cleanup:

done:        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call