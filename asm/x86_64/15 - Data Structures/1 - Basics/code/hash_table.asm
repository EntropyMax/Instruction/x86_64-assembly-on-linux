;%cflags: -f elf64
;%ldflags: -no-pie

        section .data
rfmt:   db "result: hash('%s') pointed to value %s" , 0xa, 0
dbg:    db "%lld - ", 0xa, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

a:      dq 443                  ; arbitrary prime number
b:      dq 8                    ; arbitrary table size (power of 2)
s:      db "K3", 0              ; some string
v:      db "Value B", 0         ; some other string
r:      dq 0                    ; hold the remainder as needed
p:      dq 8                    ; size of each object (char *) in the hash table
;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


         
        segment .text
        global  main
        extern printf, calloc, free


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; int hash_it(const char * key)
hash_it:
        push rbp         ; push the base pointer to onto the stack
        mov  rbp, rsp    ; move the stack pointer into the base pointer
        sub  rsp, 32     ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        
        xor rax, rax ; clear out rbx and use it as the accumulator
        xor rdx, rdx ; clear out rbx and use it as the accumulator
        xor rcx, rcx
        xor r8, r8
        xor r9, r9
        ;;;;; DONE PREPPING
        
        movzx   r8, byte [rdi+rcx]
.keep_hashing:       
        mov     rax, [a]
        mul     r8            
        add     r9, rax
        mov     rax, rdi       ; rax = a 
        inc     rcx
        movzx   r8, byte [rdi+rcx]
        cmp     r8, 0
        jne     .keep_hashing

        
        mov rax, r9           ;
        div qword [b]         ; rdx:rax = a / b  ; we're assuming that we didn't overflow
        mov qword [r], rdx    ; move remainder to [r]
        
        mov rax, rdx          ; slot return value
        
        ;;;;; CLEANUP
        leave                 ; cleanup stack frame                    
        ret                   ; return from function
    



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; char * get_it(const char * key, char** hash_table);
get_it:
        push rbp         ; push the base pointer to onto the stack
        mov  rbp, rsp    ; move the stack pointer into the base pointer
        sub  rsp, 32     ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        push r15
        ;;;;; done prepping
        
        mov  r15, rsi        ; hold a copy of the hash_table pointer in my stack frame before calling hash_it
        call hash_it         ; rdi already has key in it
                             ; offset segment is now in rax
        mul  byte [p]        ; offset segment * size of segment is now in rax - Normally would be Struc_size
        mov  rax, [r15+rax]  ; put the string in there
        
        ;;;;; cleanup
        pop   r15
        leave            ; cleanup stack frame                    
        ret   


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void set_it(const char * key, const char * value, char** hash_table);
set_it:
        push rbp             ; push the base pointer to onto the stack
        mov  rbp, rsp        ; move the stack pointer into the base pointer
        sub  rsp, 32         ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        push r15
        
        mov  r15, rdx        ; hold a copy of the hash_table pointer in my stack frame before calling hash_it
        call hash_it         ; rdi already has key in it
                             ; offset segment is now in rax
        mul  byte [p]         ; offset segment * size of segment is now in rax
        mov  [r15+rax], rsi   ; put the string in there
        
        pop     r15
        leave                ; cleanup stack frame                    
        ret   



main:
        mov  rdi, 8      ; create a hash table of 8 items
        mov  rsi, 8      ; each being a 64-bit (8 byte) pointer
        call calloc      ; and zero it out
        mov r15, rax
        
                
        lea  rdi, [s]
        lea  rsi, [v]
        mov  rdx, r15
        call set_it      ; rax holds a slot number
        
        
        lea  rdi, [s]
        mov  rsi, r15
        call get_it      ; rax holds a pointer to the string
        

        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        lea     rsi, [s]     ; parameter 2 for printf
        mov     rdx, rax     ; parameter 3 for printf
        xor     rax, rax     ; 0 floating point parameters
        call    printf

        mov  rdi, r15   
        call free        ; cleanup the hash table!
        
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        