{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='  padding: 10px; border-style: solid; background: #336600;  color: white;  background-size: 100% 100%; background-repeat: no-repeat;'>\n",
    "  <div style='  padding: 25px; text-align: center; margin: 20px; border-style: double; font-size: 30px;'>\n",
    "      <h1 style='text-shadow: -2px -2px 0 #000, 2px -2px 0 #000, -2px 2px 0 #000, 2px 2px 0 #000;'>\n",
    "          15.1.3 Hash Tables\n",
    "      </h1>\n",
    "      \n",
    "  </div>\n",
    "  <div style=' color: #EEEEEE; text-align: left; font-size: 20px;'>\n",
    "      <h2>\n",
    "          Goals:\n",
    "      </h2>\n",
    "      <ol>\n",
    "          <li>Understand the concept of a hash table</li>\n",
    "          <li>Understand the concept of key/hash collisions, including native python</li>\n",
    "          <li>Understand possible ways to manage hash collisions</li>\n",
    "          <li>Understand the how to implement a hash table in assembly</li>\n",
    "      </ol>\n",
    "  </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Refresher on Hash Tables / Dictionaries\n",
    "\n",
    "A hash table takes a given key, passes the key through an arbitrary hashing function which generates a unique *hash digest* for the key.  During inserts, the value is placed in a bucket labled by that digest. During reads, the value is read from the bucket and returned.  \n",
    "\n",
    "\n",
    "| Pre-populated hash table |\n",
    "|:-|\n",
    "|![data1](gv/perfect_hash.gv.png)|\n",
    "\n",
    "\n",
    "| Accessing a value with an existing key where `-key-`:`Value B` |\n",
    "|:-|\n",
    "|![data](gv/perfect_hash_lookup.gv.png)|\n",
    "\n",
    "\n",
    "\n",
    "### Tradeoffs\n",
    "\n",
    "\n",
    "\n",
    "Hash tables generally require substantially more *overhead* than linked lists given that it must maintain it's hash buckets in addition to the data itself.  \n",
    "\n",
    "Given a normal hash table where a key maps (through a hash function) to exactly one value, you are guaranteed constant time *O(1)* lookup times.  Compare this to a linked list where lookup times scale linearly *O(n)* with the number of items in the dataset.\n",
    "\n",
    "Hash tables are constant-time O(1) with respect to how long it takes to compute the hash given a key.  If the time required to compute the hash greater than the time it would take to inspect items in a linked list then linear O(n) would be a better choice than O(1) for a particular dataset.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Infinite input, finite output problem\n",
    "\n",
    "Regardless of the type of hash function, you will eventually find a need to deal with collisions within the output. \n",
    "\n",
    "Many hash table, dictionary, or key-value stores are constructed with a hash function to allow any arbitrary (potentially infinite) input and generate a hash digest of known size.  \n",
    "\n",
    "In the above exmaple, once there are more than 8 unique keys, we're guaranteed to see a collision given that there are only 8 possible buckets.  In that case, it would be unclear which value went with which key.  In the worst case, we'd lose values as they are overwritten and a key-read pulls back a value associated with a completely different key.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finite output problem solutions\n",
    "There are many ways to address this situation.  The most common options\n",
    "\n",
    "1. Increase the size of the hash to the point where it is astronomically unrealistic for any 2 expected values to collide\n",
    "    - change hash function from big / **md5** (infinite input to 128 bit output) to astronomical / **sha512** (infinite input to 512 bit output)\n",
    "    - incurs the cost of computing the hash during lookups (hash computation may be more expensive than a full data scan)\n",
    "2. Embrace collisions and combine a hash with a linked list\n",
    "    - hash function exists to limit scope of the problem rather than point directly to the result value\n",
    "    - descend through linked list directly comparing keys, if the key matches, return the associated value\n",
    "\n",
    "Other options not explained here:\n",
    "1. Use a Binary Tree (2 branches per node) or B-Tree (n branches per node) of hash tables (see [Chapter 15, Module 4](../../../../../notebooks/asm/x86_64/15%20-%20Data%20Structures/1%20-%20Basics/4%20-%20Binary%20Trees.ipynb) to use different hash functions at each level to narrow in on the desired value.\n",
    "2. Use a *perfect hash algorithm* where (generally) all possible keys are known up front when the table is built there is a guarantee of 1-key to 1-value.  Insertion of new keys/values is inefficient because it must rebuild the table, but Reads are highly efficient.  (Generally a good choice for historical data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Combining a hash with a linked list\n",
    "\n",
    "In situations warranting an exceedingly simple hash function prone to collisions, we can use what we learned in [Chapter 15, Module 1](../../../../../notebooks/asm/x86_64/15%20-%20Data%20Structures/1%20-%20Basics/1%20-%20Linked%20Lists.ipynb) for the value of each hash to be a linked list structure similar to below.\n",
    "\n",
    "\n",
    "There are also occasions where you want certain keys to contain more than one value without overwriting prior values.  For instace, you could use a username for a key and an ordered linked list containing all of their web comments on a particular forum. Similarly, you could use a file name as a key, and allow multiple locations on disk where that filename is observed (mycat.jpg exists in both Pictures folder and Desktop folder). \n",
    "\n",
    "\n",
    "\n",
    "| Pre-populated hash table |\n",
    "|:-|\n",
    "|![data1](gv/allow_collisions_hash.gv.png)|\n",
    "\n",
    "\n",
    "| Accessing a value with an existing key where `K5`:`Value C2` |\n",
    "|:-|\n",
    "|![data](gv/allow_collisions_hash_access.gv.png)|\n",
    "\n",
    "Upon an access for the value associated with key `K5`, our program would \n",
    "1. compute the hash value of K5 to be `6` and descend into that bucket\n",
    "2. inspect `K4`, realize that's not exactly right\n",
    "3. follow the linked list, inspect `K5`, realize that is a match\n",
    "4. return `C2` as the value \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementing a hash function\n",
    "\n",
    "Perhaps the easiest part of the process is the selection of a hash algorithm.  There are many good hash functions to choose from. In general, you initialize a hash function, pass in data (potentially one chunk/byte at a time), then observe a hash digest at the end.  \n",
    "\n",
    "A good hash function for use in adictionary should be \n",
    "1. fast - compared to list traversal\n",
    "2. collision resistant - avoid digest collisions to a reasonable degree\n",
    "3. potentially able to hash arbitrary types\n",
    "\n",
    "For instance see the python implementation of a basic hash function which can hash several different types.  Note that there is a hash collision between the numerical `1` and `boolean` true.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hash(1)=  1\n",
      "hash(True)=  1\n",
      "hash(False)=  0\n",
      "hash('1')=  -4574680760217427317\n",
      "hash(None)=  5877844869846\n"
     ]
    }
   ],
   "source": [
    "print(\"hash(1)= \", hash(1))\n",
    "print(\"hash(True)= \", hash(True))\n",
    "print(\"hash(False)= \", hash(False))\n",
    "print(\"hash('1')= \", hash(\"1\"))\n",
    "print(\"hash(None)= \", hash(None))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Note on hash collisions\n",
    "\n",
    "Notice when we use both numerical `1` and boolean `True` as keys in a python dictionary, the hash of both keys evalueates to the same digest, so we lose one of them because the other overwrite the value believing the action to be an update rather than an insert.\n",
    "\n",
    "Notice that the failure mode is extra insideous in that the key was maintained as `1`, but the value changed to `boolean`.  Unexpected hash collisions can lead to exceptionally disasterous error handling.  \n",
    "\n",
    "**Feel free to change `1` below to any other key to show the normal, non-collision behavior**\n",
    "\n",
    "This is expected behavior, not a bug in the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{1: 'boolean'}"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{\n",
    "    1: \"number\",\n",
    "    True: \"boolean\"\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hash function in assembly\n",
    "\n",
    "\n",
    "Choosing a good algorithm for an efficient hash function is a lesson on it's own and outside the scope of this module.\n",
    "\n",
    "For now, lets implement an easy hash function for short keys.\n",
    "\n",
    "An easy hash function in assembly might be multiplying by some moderate prime number and allowing the result to overflow as needed and use the modulo ([division remainder](../../../../../notebooks/asm/x86_64/06%20-%20A%20little%20bit%20of%20math/1%20-%20Basics/5%20-%20Division.ipynb#A-reminder-of-words-used)) as the position in the table.  For numerical keys, this is straightforward single multiplication.  For string keys, we can perform this calculation on each byte in the string.\n",
    "\n",
    "The prime number `443` is easy to remember since it's the port for https/tls connections."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If our hash table contains 8 items by default - given the key `K3` we can compute the offset within the hash table using the following:\n",
    "1.  K (ASCII 75) -- 75 * 443 = 33,225\n",
    "2.  3 (ASCII 51) -- 51 * 443 = 22,593\n",
    "3.  33,225 + 22,593 = 55,818\n",
    "4.  55,818 % 8 = 2\n",
    "\n",
    "The above steps would mandate the **value** for `K3` be held at offset 2 from the base.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Implementing that in assembly might look like the following.\n",
    "\n",
    "``` asm \n",
    ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n",
    "; int hash_it(const char * key)\n",
    "hash_it:\n",
    "        ;;;;; DONE PREPPING\n",
    "        \n",
    "        movzx   r8, byte [rdi+rcx]\n",
    ".keep_hashing:       \n",
    "        mov     rax, [a]\n",
    "        mul     r8            \n",
    "        add     r9, rax\n",
    "        mov     rax, rdi       ; rax = a \n",
    "        inc     rcx\n",
    "        movzx   r8, byte [rdi+rcx]\n",
    "        cmp     r8, 0\n",
    "        jne     .keep_hashing\n",
    "\n",
    "        \n",
    "        mov rax, r9           ;\n",
    "        div qword [b]         ; rdx:rax = a / b  ; divide by the number of elements in our table\n",
    "        mov qword [r], rdx    ; move remainder to [r]\n",
    "        \n",
    "        mov rax, rdx          ; slot return value\n",
    "\n",
    "\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementing the Hash Table\n",
    "\n",
    "Once you have a function to create a hash digest from an input of choice, we should begin managing our hash table.  Our hash table should be of some managable size to start, and should be a power of 2 for the sake of easy expansion.\n",
    "\n",
    "We could choose a default dictionary to store 4 keys at the start.  Once the dictionary is more than half full (or some other full metric - 2/3 full is Python's metric) we double the size of the table, recompute the hashes and store in the new locations.  When that is half full, we double again, and so on to grow the size of the table.\n",
    "\n",
    "Resizing the table incurs a substatial performance penalty as the hashes and table values need to be recomputed.\n",
    "\n",
    "\n",
    "We want the initial table size to be big enough such that we don't deal with expanding and migrating the table too often in the early stages (2, 4, 8, 16) but not so big that every instance of the hash table consumes more memory than required.  \n",
    "\n",
    "A decent choice for general purpose computing might be 8 or 16.  Specialized hash tables may initialize with more or less given knowledge of their domain.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the cpython reference implemntation indicating their choice to default hashtable size at 8 and expand the table when keys reach above half active entries\n",
    "\n",
    "``` c \n",
    "    /* PyDict_MINSIZE is the starting size for any new dict.\n",
    "     * 8 allows dicts with no more than 5 active entries; experiments suggested\n",
    "     * this suffices for the majority of dicts (consisting mostly of usually-small\n",
    "     * dicts created to pass keyword arguments).\n",
    "     * Making this 8, rather than 4 reduces the number of resizes for most\n",
    "     * dictionaries, without any significant extra memory use.\n",
    "     */\n",
    "     #define PyDict_MINSIZE 8\n",
    "```\n",
    "\n",
    "For our program we'll assume all keys have a string value.  Therefore we'll allocate 8 concurrent `char *` (which are 8 bytes a piece on the heap.  We'll use calloc to zero everything out as well.\n",
    "\n",
    "``` nasm \n",
    "        mov  rdi, 8      ; create a hash table of 8 items\n",
    "        mov  rsi, 8      ; each being a 64-bit (8 byte) pointer\n",
    "        call calloc      ; and zero it out\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setters and Getters\n",
    "\n",
    "We've crafted our hash function to spit out a number between 0 and 7 (for 8 total buckets).  \n",
    "\n",
    "The setters will simply offset to the appropriate address `bucket_number * sizeof(char*)` and set that to a given memory address.\n",
    "\n",
    "The getters will do the same, calculate the offset `bucket_number * sizeof(char*)` and return the address found there. \n",
    "\n",
    "\n",
    "In this example, neither the get or set functions perform any checking on the value doesn't already exist before continue writing or does exist during a read.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compile, Link, Execute\n",
    "\n",
    "The [full source of this example is located here](../../../../../edit/asm/x86_64/15%20-%20Data%20Structures/1%20-%20Basics/code/hash_table.asm)\n",
    "\n",
    "We'll implement a very poor hash function that splits inputs into 8 different buckets.  Reminder to understand [Chapter 9 - Functions](../../../../../tree/asm/x86_64/09%20-%20Functions/1%20-%20Basics)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! yasm \\\n",
    "    -f elf64 \\\n",
    "    -o code/hash_table.o \\\n",
    "    code/hash_table.asm && echo \"The code assembled successfully, continue to the next step\" \n",
    "! ls -alh code/hash_table.o"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! gcc \\\n",
    "    -no-pie \\\n",
    "    -o code/hash_table \\\n",
    "    code/hash_table.o\n",
    "\n",
    "! ls -alh code/hash_table*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! code/hash_table ; echo $?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! rm -rf code/hash_table.o code/hash_table\n",
    "! ls -alh code/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
