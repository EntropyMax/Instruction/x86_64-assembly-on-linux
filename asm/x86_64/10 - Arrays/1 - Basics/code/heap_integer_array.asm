        section .data
rfmt:   db "numbers[%d] = %lld" , 0xa, 0
         
        segment .text
        global  main
        extern printf
        extern calloc
        extern free


heap_create: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
 
 
 
        mov r14, 100   ; intending to hold 100 int64_t elements on the heap
        mov rdi, r14   ; parameter 1 for calloc
        mov rsi, 8     ; sizeof(int64_t) == 8 bytes
        call calloc    ; 
        mov r15, rax   ; hold onto the pointer for new memory


        mov rcx, r14
        dec rcx        ; counting is hard (0-99 not 1-100)

fill_mem:
        mov   r12, rcx            ; put the value of the counter in a working register
        imul  r12, 100            ; multiply by 100
        mov [r15+rcx*8], r12      ; register + n * m      n=counter, m=8 (quadword / 8 bytes)
        dec rcx                   ; decrement the counter
        jnz fill_mem              ; loop until zero
        
        
        lea     rdi, [rfmt]         ; parameter 1 for printf
        mov     rsi, 80             ; parameter 2 for printf (80th element)
        mov     rdx, [r15 + 80 * 8] ; parameter 3 for printf (location of array + ( 80 elements * 8 byte each )
        xor     rax, rax            ; 0 floating point parameters
        call    printf


        mov rdi, r15   ; parameter 1 for free
        call free


        leave               ; cleanup my stack frame
        ret


main:   
        call    heap_create
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call