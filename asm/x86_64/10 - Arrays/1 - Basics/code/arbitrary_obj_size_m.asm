        section .data
rfmt:   db "number at offset %d is %s" , 0xa, 0

hello:  db "  10W", 0, "  10E", 0, "  35N", 0, "  35S", 0, "1604W", 0, "1604N", 0, "1604S", 0, "1604E", 0

        segment .text
        global  main
        extern printf


func_with_local_var: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        sub rsp, 256        ; with 256 byte stack frame
 
 
 
        lea     r15, [hello+2*6]  ; label + n * m where index n=2 is chosen size m=6 is defined 
        
        
        lea     rdi, [rfmt]       ; parameter 1 for printf
        mov     rsi, 2            ; parameter 2 for printf (2nd highway name)
        mov     rdx, r15          ; parameter 3 for printf (offset calculated above)
        xor     rax, rax          ; 0 floating point parameters
        call    printf




        leave               ; cleanup my stack frame
        ret


main:   
        call    func_with_local_var
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call