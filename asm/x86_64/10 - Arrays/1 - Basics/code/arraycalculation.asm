        section .data
rfmt:   db "character at offset %d is %c" , 0xa, 0

hello:  db "Hello World!", 0
         
        segment .text
        global  main
        extern printf

func_with_local_var: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        sub rsp, 256        ; with 256 byte stack frame
        
        lea     r15, [hello]   ; get address of "Hello World!" in a temp register  
        lea     rdi, [rfmt]    ; parameter 1 for printf
        mov     rsi, 6         ; parameter 2 for printf (6th character)
        mov     rdx, [r15+6*1] ; parameter 3 for printf (location of string + ( 6 elements * 1 byte each )
        xor     rax, rax       ; 0 floating point parameters
        call    printf



        leave               ; cleanup my stack frame
        ret


main:   
        call    func_with_local_var
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call