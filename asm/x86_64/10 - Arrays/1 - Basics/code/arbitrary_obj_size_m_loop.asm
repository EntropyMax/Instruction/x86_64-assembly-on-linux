        section .data
rfmt:   db "Found a western direction - %s" , 0xa, 0

hello:  db "  10W", 0, 
        db "  10E", 0, 
        db "  35N", 0, 
        db "  35S", 0, 
        db "1604W", 0, 
        db "1604N", 0, 
        db "1604S", 0, 
        db "1604E", 0, 
        db 0,0,0,0,0,0
        ;--^ null element

        segment .text
        global  main
        extern printf


find_western_routes: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        
        lea     r15, [hello] ; r15 holds the address we're inspecting
 
more_to_process:
        mov r14, r15
        add r14, 4         ; offset 4 is 5th character (direction)
        
        mov r13b, byte [r14]
        cmp r13b, "W"                
        jne loop_termination_check
        
        lea     rdi, [rfmt]       ; parameter 1 for printf
        mov     rsi, r15          ; parameter 2 for printf (2nd highway name)
        xor     rax, rax          ; 0 floating point parameters
        call    printf

loop_termination_check:
        add r15, 6
        mov r14b, byte [r15]
        cmp r14b, 0x0               ; trusted knowledge the array ends with a null element
        jne more_to_process


        leave               ; cleanup my stack frame
        ret


main:   
        call    find_western_routes
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call