        section .data
rfmt:   db "the least value is %lld" , 0xa, 0

hello:  dq 30, 70, 20, 10, 100
         
        segment .text
        global  main
        extern printf


find_least_value: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        
        
        mov r15, [hello]    ; r15 will hold our least value (guess first element to start)
        mov r14, 1          ; r14 will be our iterator, looping [1..5)


more_to_process:        
        cmp r15, [hello+r14*8]     ; base_address + n * m  where n is the counter in r14, and m=8 byte pointer
        jl loop_termination_check  ; if r15 still less than data at base+r14*8, continue the loop without update
        mov r15, [hello+r14*8]     ; this element is now the least_value, put it in r15
        
        
loop_termination_check:
        inc r14                 
        cmp r14, 5               ; trusted knowledge the array contains 5 elements
        jne more_to_process




        lea     rdi, [rfmt]    ; parameter 1 for printf
        mov     rsi, r15         ; parameter 2 for printf (2nd number)
        xor     rax, rax       ; 0 floating point parameters
        call    printf




        leave               ; cleanup my stack frame
        ret


main:   
        call    find_least_value
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call