        section .data
rfmt:   db "arg[%d] = %s" , 0xa, 0
         
        segment .text
        global  main
        extern printf


main:   
        mov r14, rdi        ; move argc into my register
        mov r15, rsi        ; move argv into my register
        xor r13, r13        ; prep the counter
        
has_more:
        lea     rdi, [rfmt]         ; parameter 1 for printf
        mov     rsi, r13            ; parameter 2 for printf
        mov     rdx, [r15]          ; parameter 3 for printf
        xor     rax, rax            ; 0 floating point parameters
        call    printf
        inc     r13
        add     r15, 8              ;  we know that the next pointer is 8 bytes away
        cmp     r13, r14
        jne     has_more

        
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call