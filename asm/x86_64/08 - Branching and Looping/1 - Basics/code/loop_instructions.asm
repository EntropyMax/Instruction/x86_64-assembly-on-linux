        section .data
      
        segment .text
        global  main
        extern malloc
        extern free

main:   
      
        mov r14, 100000000   ; about 100 MB 
        
        
        mov rdi, r14   ; parameter 1 for malloc
        call malloc    ; get some a memory block bigger than a standard page
        

        
        mov r11, rax   ; hold onto the pointer for new memory
        mov r12, 0xdb
        
        mov rcx, r14
        dec rcx        ; counting is hard
fill_mem:
        mov [r11+rcx], r12        ; put something into memory 
        loopne fill_mem       ; decrement rcx and continue loop if still not zero
        
        mov rdi, r11   ; parameter 1 for free
        call free


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
