        section .data
      
        segment .text
        global  main
        extern malloc
        extern free

main:   
      
        mov r14, 100000000   ; about 100 MB haystack size
        mov r15, 0xDB        ; the haystack content
        mov r13, 0x03        ; the needle
        
        mov rdi, r14   ; parameter 1 for malloc
        call malloc    ; get some a memory block bigger than a standard page
        mov r12, rax   ; hold onto the pointer for new memory
        
filling_haystack:        
        mov rcx, r14         ; set the loop counter
        mov rax, r15         ; set the loop storage byte
        mov rdi, r12         ; set the destination pointer
        rep stosb            ; repeated string storage command (write 100MB to that pointer)
        ;mov [r12+2000], r13  ; place the needle somewhere in the haystack


finding_needle:
        mov rcx, r14         ; set the loop counter
        mov rax, r13         ; set the needle
        mov rdi, r12         ; set the destination pointer
        repne scasb          ; perform the scan
        je found             ; 
        mov r15, 1           ; exit code 1 means not found
        jmp cleanup          
        
found: 
        mov r15, 0           ; exit code 0 means found
        
cleanup:
        mov rdi, r12   ; parameter 1 for free
        call free

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C ; sys_exit
        mov rdi, r15  ; exit(0)
        syscall       ; execute a system call
