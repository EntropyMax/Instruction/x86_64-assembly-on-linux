        section .data
rfmt:   db "population is: %d" , 0xa, 0

      
        segment .text
        global  main
        extern printf


main:   

        mov rax, 89    ; rax holds the data
        xor rbx, rbx   ; clear since setc will fill in bl
        xor rcx, rcx   ; i = 0;
        xor rdx, rdx   ; pop = 0;
while: 
        cmp rcx, 8     ; while ( i < 8 ) {
        jnl end_while  ; requires testing on opposite
        bt rax, 0      ; data & 1
        setc bl        ; move result of test to bl
        add edx, ebx   ; pop += data & 1;
        shr rax, 1     ; data = data >> 1;
        inc rcx        ; i++;
        jmp while      ; jump backward (up) to the while loop
end_while:
        ; Continue Processing


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rdx     ; parameter 2 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
