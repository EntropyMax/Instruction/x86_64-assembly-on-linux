        section .data
rfmt:   db " =: %d", 0xa, "\n: %d" , 0xa, 0
kvstr:  db "key=value",0xa,"footer", 0
      
        segment .text
        global  main
        extern printf


main:   

        lea r10, [kvstr]    ; c[] r10 hold the address at the beginning of the string
        mov r11, 0          ; holds the byte pointed to by r10
        
        mov r13, 0          ; e   r13 holds the position of the = character
        mov r14, 0          ; n   r14 holds the position of the newline character
        
        
do_keep_looking_for_e:
        inc r10                    ; move the pointer 1 byte
        mov r11b, [r10]            ; look at c[i] (whatever single byte r10 points at)
        inc r13                    ; increment e until we find equals
        inc r14                    ; increment n until we find newline(later)
        cmp r11b, 61               ; check if we found equals sign (ascii value)
        jne do_keep_looking_for_e  ; jump back up if we have not found it

        
        
do_keep_looking_for_n:
        inc r10                    ; move the pointer 1 byte
        mov r11b, [r10]            ; look at c[i] (whatever byte r10 points at)
        inc r14                    ; increment n until we find newline
        cmp r11b, 10               ; check if we found newline sign (ascii value)
        jne do_keep_looking_for_n  ; jump back up if we have not found it
        

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, r13     ; parameter 2 for printf
        mov     rdx, r14     ; parameter 3 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
