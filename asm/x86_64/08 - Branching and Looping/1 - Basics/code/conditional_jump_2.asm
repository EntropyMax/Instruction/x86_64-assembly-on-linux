        section .data
rfmt:   db "the maximum number is: %d" , 0xa, 0

      
        segment .text
        global  main
        extern printf


main:   

        mov rax, 5
        mov rbx, 6
        mov rsi, 0       ; will hold the max value
        
        cmp rax, rbx     ; inside parenthesis of if statement
        jnl else         ; action based on the if statement
        mov rsi, rbx     ; b is the max value
        jmp endif        ; jump over the else block down to the endif label
    else: 
        mov rsi, rax
    endif:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        ;rsi is populated    ; parameter 2 for printf
        xor     eax, eax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
