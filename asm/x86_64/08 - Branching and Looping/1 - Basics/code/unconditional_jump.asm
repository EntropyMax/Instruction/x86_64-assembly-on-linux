        section .data
rfmt:   db "result: %d" , 0xa, 0

         
        segment .text
        global  main
        extern printf


main:   

        xor rbx, rbx ; clear out rbx
        xor rsi, rsi ; clear out rsi
        

        
        mov rbx, 200  ; loading 200
        jmp new_label
        mov rbx, 100  ; loading (do nothing command, jumped over it)
        
new_label:
        inc rbx       ; should be 201 now (not 101 because we skipped over it)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rbx     ; parameter 2 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
