        section .data
rfmt:   db "result: %d > %d" , 0xa, 0

      
        segment .text
        global  main
        extern printf


main:   

        mov rax, 5
        mov rbx, 6
        cmp rax, rbx
        jge already_in_order
        mov rcx, rbx
        mov rbx, rax
        mov rax, rcx
        
        
already_in_order:

        ; opted out of sorting, move on to printing

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rax     ; parameter 2 for printf
        mov     rdx, rbx     ; parameter 3 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
