        section .data
      
        segment .text
        global  main
        extern malloc
        extern free

main:   
      
        mov r14, 100000000   ; about 100 MB 


        mov rdi, r14   ; parameter 1 for malloc
        call malloc    ; get some a memory block bigger than a standard page
        mov r11, rax   ; hold onto the pointer1 for new memory
        mov r13, r11   ; hold another copy for safe keeping in a non-clobbered register
        
        mov rdi, r14   ; parameter 1 for malloc
        call malloc    ; get some a memory block bigger than a standard page
        mov r12, rax   ; hold onto the pointer1 for new memory

        dec r14
        mov rcx, r14   ; set the loop counter
        mov rax, 0xdb  ; set the loop storage byte
        mov rdi, r12   ; set the destination pointer
        mov rsi, r11   ; set the source pointer
        rep stosb      ; repeated string storage command (write 100MB to that pointer)
        
        
        mov rdi, r13   ; parameter 1 for free
        call free      ; free memory block 1
        
        mov rdi, r12   ; parameter 1 for free
        call free      ; free memory block 2
        
 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
