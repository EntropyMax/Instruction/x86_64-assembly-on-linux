        section .data
rfmt:   db "even values sum to: %d" , 0xa, 0
dbg:    db "currently inspecting: %d, %d", 0xa, 0
numbers: db 7, 1, 7, 14, 22, 38, 2, 6, 13
      
        segment .text
        global  main
        extern printf

    
    
main:   
        lea r10, [numbers]  ; numbers[] r10 hold the address at the beginning of the string
        mov r11, 0          ; holds the number pointed to by r10
        mov r12, 0          ; len_numbers
        mov r13, 0          ; counter 
        mov r14, 0          ; final_value
        
        mov r12b, [r10]     
        
while_stmt: 
        inc r13             ; counter++ in python
        cmp r13, r12        ; 
        jg break_loop       ; break statement in python
        inc r10             ; prepping accessor numbers[counter] in python
        mov r11b, [r10]     ; accessing the number
        
        xor rax, rax        ; prepare for div
        xor rdx, rdx        ; prepare for div
        mov rax, r11        ; prepare for div
        mov rbx, 2          ; prepare for div
        idiv rbx            ; numbers % 2
        cmp rdx, 1          ; (numbers % 2 == 1)
        je while_stmt       ; continue statement in python
        add r14, r11        ; final_value += numbers[counter]
        jmp while_stmt      ; implicit loop statement (indenting in py, close curly brace in C)

break_loop:




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, r14     ; parameter 2 for printf
        xor     eax, eax    ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
