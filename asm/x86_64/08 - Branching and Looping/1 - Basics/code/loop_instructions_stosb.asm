        section .data
      
        segment .text
        global  main
        extern malloc
        extern free

main:   
      
        mov r14, 100000000   ; about 100 MB 
        
        
        mov rdi, r14   ; parameter 1 for malloc
        call malloc    ; get some a memory block bigger than a standard page
        mov r11, rax   ; hold onto the pointer for new memory
        
        
        mov rcx, r14   ; set the loop counter
        mov rax, 0xdb  ; set the loop storage byte
        mov rdi, r11   ; set the destination pointer
        rep stosb      ; repeated string storage command (write 100MB to that pointer)
        
        mov rdi, r11   ; parameter 1 for free
        call free

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
