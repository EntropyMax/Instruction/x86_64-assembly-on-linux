        section .data
rfmt:   db "result: %d = %d - %d" , 0xa, 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

a:      dq 151
b:      dq 310

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         
        segment .text
        global  main
        extern printf


main:   
        xor rbx, rbx ; clear out rbx and use it as the accumulator
        
        
        
                     ; rbx = 0
        mov rbx, [a] ; rbx = a 
        sub rbx, [b] ; rbx = a + b





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, rbx     ; parameter 2 for printf
        mov     rdx, [a]     ; parameter 3 for printf
        mov     rcx, [b]     ; parameter 4 for printf
        xor     eax, eax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
