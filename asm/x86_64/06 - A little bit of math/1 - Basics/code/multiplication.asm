        section .data
rfmt:   db "result: (%d << 64) + %d  = %d * %d" , 0xa, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

a:      dq 120
b:      dq 2
m:      dq 0
l:      dq 0

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


         
        segment .text
        global  main
        extern printf


main:   
        xor rax, rax ; clear out rbx and use it as the accumulator
        xor rdx, rdx ; clear out rbx and use it as the accumulator
        
        
        
                     ; rax = 0
        mov rax, [a] ; rax = a 
        
        mul qword [b] ; rdx:rax = a * b  ; we're assuming that we didn't overflow

        mov [m], rdx  ; move MSB into memory
        mov [l], rax  ; move LSB into memory


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, [m]     ; parameter 2 for printf
        mov     rdx, [l]     ; parameter 3 for printf
        mov     rcx, [a]     ; parameter 4 for printf
        mov     r8 , [b]     ; parameter 4 for printf
        xor     eax, eax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        