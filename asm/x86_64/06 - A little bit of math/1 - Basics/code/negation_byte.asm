        section .data
rfmt:   db "result: %d" , 0xa, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

sample: db 12

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        segment .text
        global  main
        extern printf


main:   
        
        neg byte [sample]  ; should now be -12 (but we need to sign extend!)
        


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            
        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        
        lea     rdi, [rfmt]       ; parameter 1 for printf
        movsx   rsi, byte [sample]; parameter 2 for printf
        xor     eax, eax          ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
