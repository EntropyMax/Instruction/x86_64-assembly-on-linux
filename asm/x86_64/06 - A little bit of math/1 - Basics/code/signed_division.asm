        section .data
rfmt:   db "result: %d / %d = quotient %d and remainder %d" , 0xa, 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

a:      dq -10
b:      dq 3
q:      dq 0
r:      dq 0

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         
        segment .text
        global  main
        extern printf


main:   
        xor rax, rax ; clear out rbx and use it as the accumulator
        xor rdx, rdx ; clear out rbx and use it as the accumulator
        
                     ; rax = 0
        mov rax, [a] ; rax = a 
        cqo          ; sign extend  
        
        idiv qword [b] ; rdx:rax = a / b  ; we're assuming that we didn't overflow

        mov qword [q], rax    ; move quotient to [q]
        mov qword [r], rdx    ; move remainder to [r]



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Support Content Below ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; Don't worry about calling the printf function here, we'll get to that in chapter 9
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, [a]     ; parameter 2 for printf
        mov     rdx, [b]     ; parameter 3 for printf
        mov     rcx, [q]     ; parameter 4 for printf
        mov     r8 , [r]     ; parameter 5 for printf 
        xor     eax, eax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
