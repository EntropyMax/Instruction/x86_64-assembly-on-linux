;   Program: exit_only
;
;   Executes the exit system call
;
;   No input
;
;   Output: only the exit status ($? in the shell)
;
    segment .text
    global _start
_start:
    mov rax, 0x3C ; 0x3C is the exit syscall number (don't worry we'll get to that later)
    mov rdi,5     ; the status value to return
    syscall       ; execute a system call