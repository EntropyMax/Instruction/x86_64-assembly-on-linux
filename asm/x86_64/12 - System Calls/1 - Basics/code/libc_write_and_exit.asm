        section .data
hello:  db "hello world!" , 0xa, 0

        segment .text
        global  main
        extern write, exit    ; note external symbols required in lieu of sys_* versions
        
main:   
                              ; rax not required since we're suing libc
        mov     rdi, 1        ; File descriptor for stdout
        lea     rsi, [hello]  ; parameter 2 for sys_write
        mov     rdx, 13       ; length of the string "hello world!\n"
        call    write         ; invoke the system call


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Standard 64-bit exit  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov  rdi,0            ; exit(0)
        call exit             ; execute a system call
        