        section .data
hello:  db "hello world!" , 0xa, 0

        segment .text
        global  _start

_start:   
        mov     rax, 0x01     ; sys_write command into eax
        mov     rdi, 1        ; File descriptor for stdout
        lea     rsi, [hello]  ; parameter 2 for sys_write
        mov     rdx, 13       ; length of the string "hello world!\n"
        syscall               ; invoke the system call


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Standard 64-bit exit  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C        ; sys_exit 
        mov rdi,0            ; exit(0)
        syscall              ; execute a system call
        