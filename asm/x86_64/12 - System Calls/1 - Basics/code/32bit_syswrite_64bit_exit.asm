        section .data
hello:  db "hello world!" , 0xa, 0

        segment .text
        global  _start

_start:   
        mov     eax, 0x04     ; sys_write command into eax
        mov     ebx, 1        ; File descriptor for stdout
        lea     ecx, [hello]  ; parameter 2 for sys_write
        mov     edx, 13       ; length of the string "hello world!\n"
        int     0x80          ; invoke the system call


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Standard 64-bit exit  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        