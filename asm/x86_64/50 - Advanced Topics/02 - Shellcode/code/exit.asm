global _start
section .text


_start:
    mov  eax, 0xFFFFFFFF   ; 1 is the exit syscall number
    xor  eax, 0xFFFFFFFE   ; 
    mov  ebx,0xFFFFFFFF    ; 0 the status value to return
    xor  ebx, ebx          ;
    int  0x80              ; execute a system call

