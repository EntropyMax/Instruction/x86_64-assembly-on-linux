section     .text
global      _start                        

_start:                                         ;tell linker entry point

    mov     rdx,len                             ;message length
    mov     rsi,msg                             ;message to write
    mov     rdi,1                               ;file descriptor (stdout)
    mov     rax,1                               ;system call number (sys_write)
    syscall                                     ;call kernel

    mov     eax,60                              ;system call number (sys_exit)
    mov     rdi,0                               ;error code = 0
    syscall

section     .data

msg     db  'Hello, world!',0xa                 ;our dear string
len     equ $ - msg                             ;length of our dear string