global _start

_start:
    jmp setup

run:  
    pop r8              ; snag the file content
    mov r14, r8         ; r14 holds the filename of the target
    xor r9, r9          ; zero out whatever wass in there since we're just using LSB
    mov r9b, [r8]       ; r9b holds the size of the content body
    xor r10, r10        ; r10 holds the length of the content body
    
    ; SPLIT THE FILE CONTENT FROM THE FILE NAME
    
waiting_for_go:
    inc r14
    inc r10
    sub r9, 1
    jnz waiting_for_go
    
    
    ; CREATE THE FILE
    xor rax, rax            ; zero out clobberd registers
    xor rdx, rdx            ; zero out clobberd registers
    xor esi, esi            ; zero out clobberd registers
    mov ax, 0xFFF2
    xor ax, 0xFFF0
    mov dx, 777o
    mov si, 0x242
    mov rdi, r14
    syscall                ; gogogogogo
    
    
    mov     r15, rax        ; put the id in the register for close
    
    
    ; WRITE SOME CONTENT INTO THE FILE
    xor rax, rax           ; write = 1
    inc rax
    
    mov rsi, r8     ;
    mov rdi, r15    ; file descriptor
    mov rdx, r10
    
    ; we need to hop over the size byte and decrement the count byte
    inc rsi
    dec rdx
    syscall 
    
    
    ; CLOSE THE FILE

    xor rax, rax           ; close file = 3
    inc rax
    inc rax
    inc rax
    mov rdi, r15
    syscall                ; gogogogo
    
    
    ; Safe close handling.
    xor     rax, rax      
    mov     ax,  0xFF3D   ;obfuscation technique fill rax and subtract to result in expected value
    sub     ax,  0xFF01   ;system call number (sys_exit) = 60 is now in eax
    xor     rdi, rdi      ;obfuscation technique to clear rdi, resulting in error code = 0
    syscall
    
setup:
    call run
    db  0x0A, 'hi there!', '/tmp/bauman'  
    ;    ^
    ;    This is the length of the content, including the size byte  "hi there!" is 9 bytes, so size is 10
    
    
     
    


