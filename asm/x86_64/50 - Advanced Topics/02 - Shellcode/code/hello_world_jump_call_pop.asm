section     .text
global      _start                        

_start:                         ;tell linker entry point
    jmp setup
    
run:
    pop     r11        ; This is now the address for db  'Hello, world!',0xa
    mov     r12, r11   ; use r12 to "walk" the string
    xor     r13b, r13b ; use to hold the character at this position
    xor     r14, r14   ; making our own accumulator
collecting_len:
    inc r14            ; we're inspecting a byte here
    mov r13b, [r12]    ; move whatever byte is at the walking pointer into r13
    inc r12            ; increment the walking pointer
    sub r13b, 0xa      ; is this our stop byte?
    jnz collecting_len
    
    
    ; all the real code
    mov     rdx, r14      ;message length
    mov     rsi, r11      ;message to write
    xor     rdi, rdi      ;file descriptor (stdout)
    inc     rdi           ; obfuscation technique to clear rdi and increment, resulting in a 1 in rdi
    xor     rax, rax      ;system call number (sys_write)
    inc     rax           ; obfuscation technique to clear rax and increment, resulting in a 1 in rax
    syscall               ;call kernel
    
    xor     rax, rax      
    mov     ax,  0xFF3D   ;obfuscation technique fill rax and subtract to result in expected value
    sub     ax,  0xFF01   ;system call number (sys_exit) = 60 is now in eax
    xor     rdi, rdi      ;obfuscation technique to clear rdi, resulting in error code = 0
    syscall


setup:
   call run                      
   db  'Hello, world!',0xa      ; the address for this will be on the stack during the run call
  
  