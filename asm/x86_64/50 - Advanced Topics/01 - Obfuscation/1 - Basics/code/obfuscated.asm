global _start
section .text

;;;;;;; Example 32-bit sys_exit call
;;;;;;; Load eax with 1 and ebx with 0

_start:

    mov  eax, 1     ; 1 is the exit syscall number
    mov  ebx, 0     ; 0 the status value to return
    int  0x80       ; execute a system call

