global _start
section .text


;;;;;;; Example 32-bit sys_exit call
;;;;;;; Load eax with 1 and ebx with 0

_start:
    mov  eax, 0xFFFFFFFF   ; 1 is the exit syscall number
    sub  eax, 0xFFFFFFFE   ; 
    mov  ebx, 0xFFFFFFFF   ; 0 the status value to return
    sub  ebx, 0xFFFFFFFF   ;
    int  0x80              ; execute a system call


