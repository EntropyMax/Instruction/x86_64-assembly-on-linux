global _start
section .text

_start:

    mov  eax, 0xDBDBDBDB   ; 1 is the exit syscall number
    sub  eax, 0xFFFFFFFE    
    mov  ebx, 0xDBDBDBDB    ; 0 the status value to return
    sub  ebx, 0xFFFFFFFF   
    int  0x80              ; execute a system call
