global _start
section .text

_start:

    mov  eax, 0xDBDBDBDB   ; 1 is the exit syscall number
                           ; SOMETHING GOES HERE
    mov  ebx,0xFFFFFFFF    ; 0 the status value to return
                           ; SOMETHING GOES HERE
    int  0x80              ; execute a system call
