global _start
section .text

;;;;;;; Example 32-bit sys_exit call
;;;;;;; Load eax with 1 and ebx with 0

_start:

    ; Try using the div instruction
    ; reminder: dividend needs to be in eax
    ;           divisior should be in another register
    ;           quotient will be in eax
    ;           remainder will be in edx
    ;           consider clearing edx between divisions
    
    ; hint, you know you want 1 in eax and 0 in ebx at the syscall
    ; consider the order
    
    
    mov eax, 0xDBDBDBDB
    mov ecx, 0xFFFFFFFF
    div dword ecx
    mov ebx, eax ; 0 the status value to return
    
    mov edx, 0xFFFFFFFF
    xor edx, edx
    
    mov  eax, 0xDBDBDBDB   
    mov  ecx, 0xDBDBDBDB
    div  dword ecx         ; 1 is the exit syscall number
    

    int  0x80              ; execute a system call

