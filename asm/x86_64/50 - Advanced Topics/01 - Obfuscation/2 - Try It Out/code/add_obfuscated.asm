global _start
section .text

;;;;;;; Example 32-bit sys_exit call
;;;;;;; Load eax with 1 and ebx with 0


_start:

    mov  eax, 0xDCBDCBDB   ; 1 is the exit syscall number
    add  eax, 0x24242426   ;  probably needs changed
    mov  ebx, 0xDCBDCBDB    ; 0 the status value to return
    add  ebx, 0x24242426   ; probably needs changed
    int  0x80              ; execute a system call


