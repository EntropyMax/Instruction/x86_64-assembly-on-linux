global _start
section .text


;;;;;;; Example 32-bit sys_exit call
;;;;;;; Load eax with 1 and ebx with 0

_start:

    ; consider the inc or dec instruction
    
    ; hint, you know you want 1 in eax and 0 in ebx at the syscall

    
    mov eax, 0xDBDBDBDB  ; eventually you want this to be 1
    mov ebx, 0xDBDBDBDB  ; eventually you want this to be 0


keep_going_eax: 
    dec eax
    cmp eax, 1
    jne keep_going_eax  
    
keep_going_ebx: 
    dec ebx
    cmp ebx, 1
    jne keep_going_ebx
    dec ebx
    

    int  0x80              ; execute a system call

