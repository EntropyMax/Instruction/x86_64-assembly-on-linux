        section .data

         struc      Pricing 
p_id     resq       1
p_price  resq       1
         endstruc

count:   dq 25
range:   dq 1100

        segment .text
        global  main
        extern srand, calloc, free, populate, query


main:   
        mov  rsi, 0xdbdb   ; use the same seed every time to
        call srand         ; generate a repeatable number sequence
        
        
        
        mov  rdi, Pricing_size ; automatically generated label from the compiler
        mov  rsi, [count]      ; global count label holds 25
        call calloc
        mov  r15, rax          ; hold the pointer so we can call printf
        mov  r12, rax          ; and an extra backup too
        
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Calls to External Objects
        ; populate(int count, struct Pricing * pricing[])
        mov rdi, [count]
        mov rsi, r15
        call populate
        
        
        mov rdi, [range]  ; price range
        mov rsi, [count]
        mov rdx, r15
        call query
        
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        
        mov     rdi, r12
        call    free

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        