        section .data
rfmt:   db "populating %lld with $%lld" , 0xa, 0

         struc      Pricing 
p_id     resq       1
p_price  resq       1
         endstruc


reduce:  dq 1000000


        segment .text
        global  populate
        extern rand, printf


; populate(int count, struct Pricing * pricing[])
populate:
        push rbp         ; push the base pointer to onto the stack
        mov  rbp, rsp    ; move the stack pointer into the base pointer
        sub  rsp, 32     ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        push r15         ; preserve volatile registers
        push r14
        push r13
        push r12
        mov  r14, rdi                 ; parameter 0 of this function (count of elements in array) (trustworthy?)
        mov  r15, rsi                 ; parameter 1 of the function (struct array base pointer)
        xor  r13, r13                 ; holds the incrementing counter
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
populating:
        call rand                     ; get a 'random' number into rax
        xor rdx, rdx                  ; prep for division (see Chapter 6)
        div qword [reduce]            ; global with magic number to get realistic numbers
        
        mov [r15+p_id],    r13        ;  r13 simply an iterating counter to generate a property id
        mov [r15+p_price], rax
        
        

        lea     rdi,  [rfmt]            ; parameter 1 for printf
        mov     rsi,  [r15 + p_id]
        mov     rdx,  [r15 + p_price]
        xor     rax, rax                ; 0 floating point parameters
        call    printf    
        
        
        
        add r15, Pricing_size        ; move the walking pointer to the start of the next struct
        inc r13                      ; increment our counter
        cmp r13, r14                 ; count passed in as parameter 1
        jne populating               ; keep counting if not at the desired count
                
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        pop r12  ; restore volatile registers
        pop r13
        pop r14
        pop r15
        leave    ; cleanup stack frame                    
        ret      ; return from function