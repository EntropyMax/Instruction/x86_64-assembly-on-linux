        section .data
rdcb:   db "location %lld which costs $%lld is affordable on a budget of $%lld", 0xa, 0

         struc      Pricing 
p_id     resq       1
p_price  resq       1
         endstruc


        segment .text
        global  query
        extern printf

; query(int max_price, int count, struct Pricing * pricing[])
query: 
        push rbp         ; push the base pointer to onto the stack
        mov  rbp, rsp    ; move the stack pointer into the base pointer
        sub  rsp, 32     ; subtract arbitrary (must be 16 byte aligned) space from the stack pointer to reserve
        push r15         ; preserve volatile registers
        push r14
        push r13
        mov  r13, rdi                 ; parameter 0 of this function (max rental price for user)
        mov  r14, rsi                 ; parameter 1 of this function (count of elements in array) (trustworthy?)
        mov  r15, rdx                 ; parameter 2 of the function (struct array base pointer)
        xor  rbx, rbx                 ; use a volatile counter this time to save a push!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

find_affordable:
        cmp     [r15 + p_price], r13    ; r13 holds a max monthly price that is affordable
        jg      find_loop_termination   ; don't print if this property is over budget
        lea     rdi,  [rdcb]            ; parameter 1 for printf
        mov     rsi,  [r15 + p_id]      ; parameter 2 for printf
        mov     rdx,  [r15 + p_price]   ; parameter 3 for printf
        mov     rcx,  r13               ; parameter 4 for printf
        xor     rax, rax                ; 0 floating point parameters
        call    printf 
find_loop_termination:
        add     r15,  Pricing_size      ; move the walking pointer along
        inc     rbx                     ; increase the counter
        cmp     rbx, r14                ; check the counter against max count
        jne     find_affordable         ; continue looping if we have more to go

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        
        pop r13  ; restore volatile registers
        pop r14
        pop r15
        leave    ; cleanup stack frame                    
        ret      ; return from function

        
