#include <stdlib.h>     /* atoi */
#include <stdio.h>

// These will be found in the assembly at link time!
void populate(int, void*);
void query(int, int, void*);

int main(int argc,  char *argv[]){
    int limit = atoi(argv[1]);
    printf("you wanted a limit of %d\n", limit);
    
    // Totally opaque object, no idea what it is!
    void* some_memory = malloc(100000); //hope thats enough!
    
    
    populate(25, some_memory);
    query(limit, 25, some_memory);
    
    
    free(some_memory);
    
    return 0;    
}