        section .data
rfmt:   db "populating %lld with $%lld" , 0xa, 0
rdcb:   db "location %lld which costs $%lld is affordable on a budget of $%lld", 0xa, 0

         struc      Pricing 
p_id     resq       1
p_price  resq       1
         endstruc

count:   dq 25
reduce:  dq 1000000
range:   dq 1100

        segment .text
        global  main
        extern printf, rand, srand, calloc, free


main:   
        mov  rsi, 0xdbdb   ; use the same seed every time to
        call srand         ; generate a repeatable number sequence
        
        
        
        mov  rdi, Pricing_size ; automatically generated label from the compiler
        mov  rsi, [count]      ; global count label holds 25
        call calloc
        mov  r15, rax          ; hold the pointer so we can call printf
        mov  r12, rax          ; and an extra backup too
        

        xor  r13, r13
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;        
populating:
        call rand                     ; get a 'random' number into rax
        xor rdx, rdx                  ; prep for division (see Chapter 6)
        div qword [reduce]            ; global with magic number to get realistic numbers
        
        mov [r15+p_id],    r13        ;  r13 simply an iterating counter to generate a property id
        mov [r15+p_price], rax
        
        

        lea     rdi,  [rfmt]            ; parameter 1 for printf
        mov     rsi,  [r15 + p_id]
        mov     rdx,  [r15 + p_price]
        xor     rax, rax                ; 0 floating point parameters
        call    printf    
        
        
        
        add r15, Pricing_size        ; move the walking pointer to the start of the next struct
        inc r13                      ; increment our counter
        cmp r13, [count]             ; global count label holds 25
        jne populating               ; keep counting if not at the desired count
        
        
        xor     rbx, rbx
        mov     r13, [range]
        mov     r15, r12
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
find_affordable:
        cmp     [r15 + p_price], r13    ; r13 holds a max monthly price that is affordable
        jg      find_loop_termination   ; don't print if this property is over budget
        lea     rdi,  [rdcb]            ; parameter 1 for printf
        mov     rsi,  [r15 + p_id]      ; parameter 2 for printf
        mov     rdx,  [r15 + p_price]   ; parameter 3 for printf
        mov     rcx,  r13               ; parameter 4 for printf
        xor     rax, rax                ; 0 floating point parameters
        call    printf 
find_loop_termination:
        add     r15,  Pricing_size      ; move the walking pointer along
        inc     rbx                     ; increase the counter
        cmp     rbx, [count]            ; check the counter against max count
        jne     find_affordable         ; continue looping if we have more to go
        
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;        
        mov     rdi, r12
        call    free

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        