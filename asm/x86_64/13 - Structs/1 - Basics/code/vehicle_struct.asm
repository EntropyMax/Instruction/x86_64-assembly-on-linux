        section .data
rfmt:   db "$%d - %hd %s %s" , 0xa, 0

example_make:  db "DMC", 0, 0
example_model: db "DeLorean", 0, 0
example_year:  dw 1982
example_price: dd 50000


         struc      Vehicle 
v_id     resq       1
v_price  resd       1
v_year   resw       1
v_m_o_l  resb       1  
v_comi   resb       1
v_make   resb       32
v_model  resb       32
         endstruc

        segment .text
        global  main
        extern printf, strncpy, calloc, free

main:   
        mov  rdi, Vehicle_size
        mov  rsi, 1
        call calloc
        mov  r15, rax          ; hold the pointer so we can call printf
        
        mov r14d,  [example_price]
        mov [r15 + v_price], r14d
        
        mov r14w, [example_year]
        mov [r15 + v_year],  r14w  
        
        lea rdi, [r15 + v_make]
        lea rsi, [example_make]
        mov rdx, 31
        call strncpy
        
        
        lea rdi, [r15 + v_model]
        lea rsi, [example_model]
        mov rdx, 31
        call strncpy
        
        
        
        
        
        lea     rdi,  [rfmt]                 ; parameter 1 for printf
        mov     rsi,  [r15 + v_price]
        mov     rdx,  [r15 + v_year]
        lea     rcx,  [r15 + v_make]
        lea     r8,   [r15 + v_model]
        xor     rax, rax                    ; 0 floating point parameters
        call    printf
        
        
        
        mov     rdi, r15
        call    free

        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call
        