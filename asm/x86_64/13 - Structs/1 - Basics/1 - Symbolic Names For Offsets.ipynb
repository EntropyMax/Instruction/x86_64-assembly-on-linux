{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='  padding: 10px; border-style: solid; background: #336600;  color: white;  background-size: 100% 100%; background-repeat: no-repeat;'>\n",
    "  <div style='  padding: 25px; text-align: center; margin: 20px; border-style: double; font-size: 30px;'>\n",
    "      <h1 style='text-shadow: -2px -2px 0 #000, 2px -2px 0 #000, -2px 2px 0 #000, 2px 2px 0 #000;'>\n",
    "          13.1.1 Symbolic Names For Offsets\n",
    "      </h1>\n",
    "      \n",
    "  </div>\n",
    "  <div style=' color: #EEEEEE; text-align: left; font-size: 20px;'>\n",
    "      <h2>\n",
    "          Goals:\n",
    "      </h2>\n",
    "      <ol>\n",
    "          <li>Understand how to hand-calculate the size of an unpadded struct</li>\n",
    "          <li>Understand how to define a struct in Assembly using struc/endstruc</li>\n",
    "          <li>Understand both global and struc-specific symbolic identifiers and how to implement</li>\n",
    "      </ol>\n",
    "  </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review of Structs\n",
    "\n",
    "A C struct is a heterogeneous object, meaning one struct can be made up of many different component types.  \n",
    "\n",
    "For example, a used car lot may store each vehicle in a struct that looks as follows. \n",
    "\n",
    "**Note** Optimizing Struct layouts to avoid struct padding which allows for aligned read/writes is out of scope of this module. Be aware that struct padding occurs automatically with optimizing compilers like gcc.  The sum of component sizes may be less than the the actual size of the struct if `gcc` applied struct padding.\n",
    "\n",
    "``` c \n",
    "struct Vehicle {\n",
    "    uint64_t id;               //  8 bytes  (qw)\n",
    "    uint32_t price;            //  4 bytes  (dw)\n",
    "    uint16_t year;             //  2 bytes  (w)\n",
    "    uint8_t  months_on_lot;    //  1 bytes  (b)\n",
    "    uint8_t  comission;        //  1 bytes  (b)\n",
    "    char     make[32];         // 32 bytes  (b)[32] \n",
    "    char     model[32];        // 32 bytes  (b)[32]\n",
    "};\n",
    "```\n",
    "\n",
    "We can add up the sizes of the individual components to obtain the struct size:\n",
    "\n",
    "$(8+4+2+1+1+32+32) = 80$\n",
    "\n",
    "C standard libraries provide a function `sizeof` to to obtain this size automatically which includes struct padding if any was used during compilation.\n",
    "\n",
    "``` c \n",
    "printf(\"%ld\\n\", sizeof(struct Vehicle));  // prints 80\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure Definition in Assembly\n",
    "\n",
    "Use the `struc` instruction to begin a struct definition and `endstruc` to end the struct definition \n",
    "\n",
    "*note the difference `struc` in assembly versus `struct` in C*\n",
    "\n",
    "The general format of a struct definition looks like the following\n",
    "``` nasm \n",
    "        struc  <struc_name>\n",
    "            ...\n",
    "            <struct components>\n",
    "            ...\n",
    "        endstruc\n",
    "```\n",
    "\n",
    "Assembly compilers will also generate a struct size during compilation.  A automatically generated label will be the struct name with a `_size` suffex.\n",
    "\n",
    "**note** C-compilers will re-align (but never re-arrange) the components of a structure.  Similar alignment in Assembly must be performed manually.  It may be useful to compare the results of `sizeof` to the `<name>_size` value to determine if your manual alignment in assembly is as optimized as what is produced by a c compiler. \n",
    "\n",
    "A common method to allocate heap space for a struct is to use the following\n",
    "``` nasm \n",
    "    mov   rdi, <struc_name>_size\n",
    "    call  malloc\n",
    "    ; rax has a pointer to memory at least as big as the size of the struct, without needing to hand-calculate\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Struct Components\n",
    "\n",
    "Each struct component is made up of `label  res_  ##` where\n",
    "- `label` is any descriptive name for the reservation\n",
    "- `res_` is the reserved size (`resb`=1byte, `resw`= 2 bytes, `resd`=4 bytes, `resq`=8 bytes, ... )\n",
    "- `##` is number of reservations of the defined size for this label"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global Symbolic component names\n",
    "\n",
    "You may choose any label to define each component of the struct.  \n",
    "\n",
    "The labels shown here will be global, meaning any other segment of the code can use the label to aquire the offset to access the labeled component within the struct.\n",
    "\n",
    "Mapping the C-struct defined above into assembly might look like this. \n",
    "``` nasm\n",
    "        struc      Vehicle \n",
    "id      resq       1\n",
    "price   resd       1\n",
    "year    resw       1\n",
    "m_o_l   resb       1  \n",
    "comi    resb       1\n",
    "make    resb       32\n",
    "model   resb       32\n",
    "        endstruc\n",
    "```\n",
    "\n",
    "Accessing `price` label, which exists after 8 bytes after the id label because the id reserved 8 bytes.\n",
    "\n",
    "For instance if the base address for this struct was in r10, `mov r11, r10+price` would add 8 (`id resq 1`=8) to `r10` and place the result in `r11` \n",
    "\n",
    "As with any other global label, you may not re-use identifiers. These global labels reserve `id`, `price`, etc as the only times you may use these labels in your program.  Programs with multiple structs may want an identifier field `id` in all struct definitions, complicating the naming conventions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Struct-specific Symbolic component names\n",
    "\n",
    "Struct-specific labels are a solution to re-use problem.\n",
    "\n",
    "Prepending a period/dot to each label, makes the label specific to this particular struct.\n",
    "\n",
    "``` nasm\n",
    "        struc      Vehicle \n",
    ".id     resq       1\n",
    ".price  resd       1\n",
    ".year   resw       1\n",
    ".m_o_l  resb       1  \n",
    ".comi   resb       1\n",
    ".make   resb       32\n",
    ".model  resb       32\n",
    "        endstruc\n",
    "```\n",
    "\n",
    "You accessing these labels using the automatically generated label following the naming convention `StrucName.label`.\n",
    "\n",
    "Accessing `.price` label becomes `Vehicle.price`.\n",
    "\n",
    "In the same scenario above, you would use `mov r11, r10+Vehicle.price`.\n",
    "\n",
    "The drawback to this method is the verbosity. While this method makes the label abundantly clear, concise but readable source should be a design goal of a well-built assembly file.  Long struct names prepended to each access may result in lower reader comprehension.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Common Compromise using prefixed global labels.\n",
    "\n",
    "Frequently, developers compromise by prepending a short `initial_` for the description of the label.  \n",
    "\n",
    "This method provides clarity to the code-reviewer, avoids a reasonable amount of label re-use problems, keeps the labels short, and avoids using compiler auto-generated labels (*less of a problem now that static code analyzers are able to consider these generated labels that don't exist in the source*)\n",
    "\n",
    "``` nasm\n",
    "         struc      Vehicle \n",
    "v_id     resq       1\n",
    "v_price  resd       1\n",
    "v_year   resw       1\n",
    "v_m_o_l  resb       1  \n",
    "v_comi   resb       1\n",
    "v_make   resb       32\n",
    "v_model  resb       32\n",
    "         endstruc\n",
    "```\n",
    "\n",
    "In the same scenario above, you would use `mov r11, r10+v_price`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choosing the correct label method\n",
    "Ultimately, the choice comes down to developer and team preference bound by a team/contributor agreement.  \n",
    "\n",
    "Programmers coming from higher level languages may appreciate verbosity and choose struct-specific naming with automatically generated labels. \n",
    "\n",
    "Programmers with historical background with linux may be inclined to follow the Linux Kernel code styling guide for absolute minimum length label names.\n",
    "\n",
    "\n",
    "<div style=\"font: 10px/1.2 Courier, monospace;\">\n",
    "<br>\n",
    "    C is a Spartan language, and so should your naming be. Unlike Modula-2 and Pascal programmers, C programmers do not use cute names like ThisVariableIsATemporaryCounter. A C programmer would call that variable tmp, which is  much easier to write, and not the least more difficult to understand.\n",
    "<br>\n",
    "...\n",
    "<br>\n",
    "LOCAL variable names should be short, and to the point. If you have some random integer loop counter, it should probably be called i. Calling it loop_counter is non-productive, if there is no chance of it being mis-understood. Similarly, tmp can be just about any type of variable that is used to hold a temporary value.\n",
    "</div>\n",
    "\n",
    "**External Link** https://www.kernel.org/doc/html/v4.10/process/coding-style.html#naming\n",
    "\n",
    "The linux kernel community is extremely opinionated and speak with some degree of authority on style guides (especially within their project), but there is not a correct and incorrect answer beyond writing code agreeable to the team you're on and the project you're contributing to."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compile, Link, Execute\n",
    "\n",
    "The entire assembly file can be opened with this [modifiable version of the code](../../../../../edit/asm/x86_64/13%20-%20Structs/1%20-%20Basics/code/vehicle_struct.asm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we use the calloc function to allocate enough memory for exactly 1 struct on the heap and use `r15` as our base pointer.  Calloc also zeros out the entire memory area for us so we don't have lingering bad content. \n",
    "\n",
    "``` nasm \n",
    "        mov  rdi, Vehicle_size\n",
    "        mov  rsi, 1\n",
    "        call calloc\n",
    "        mov  r15, rax          ; hold the pointer so we can call printf\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, in an example using different integer sizes, we move the contents at the label to the appropriate register\n",
    "\n",
    "``` nasm \n",
    "        mov r14d,  [example_price]\n",
    "        mov [r15 + v_price], r14d\n",
    "        \n",
    "        mov r14w, [example_year]\n",
    "        mov [r15 + v_year],  r14w \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we do a safe string copy from the example make/model into the appropriate struct components.  We set the maximum for strncpy at 31 so we will maintain our null pointer at the end, even if the example strings are too big. \n",
    "\n",
    "``` nasm\n",
    "        lea rdi, [r15 + v_make]\n",
    "        lea rsi, [example_make]\n",
    "        mov rdx, 31\n",
    "        call strncpy\n",
    "        \n",
    "        \n",
    "        lea rdi, [r15 + v_model]\n",
    "        lea rsi, [example_model]\n",
    "        mov rdx, 31\n",
    "        call strncpy\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we prepare the printf where we load two different-sized numbers as the first parameters, then load the effective address `lea` for the strings before finally freeing our struct\n",
    "\n",
    "``` nasm\n",
    "        lea     rdi,  [rfmt]                 ; parameter 1 for printf\n",
    "        mov     rsi,  [r15 + v_price]\n",
    "        mov     rdx,  [r15 + v_year]\n",
    "        lea     rcx,  [r15 + v_make]\n",
    "        lea     r8,   [r15 + v_model]\n",
    "        xor     rax, rax                    ; 0 floating point parameters\n",
    "        call    printf\n",
    "        \n",
    "        mov     rdi, r15\n",
    "        call    free\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! yasm \\\n",
    "    -f elf64 \\\n",
    "    -o code/vehicle_struct.o \\\n",
    "    code/vehicle_struct.asm && echo \"The code assembled successfully, continue to the next step\" \n",
    "\n",
    "\n",
    "! ls -alh code/vehicle_struct.o"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! gcc \\\n",
    "    -no-pie \\\n",
    "    -o code/vehicle_struct \\\n",
    "    code/vehicle_struct.o\n",
    "\n",
    "! ls -alh code/vehicle_struct*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! code/vehicle_struct  ; echo $?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! rm -rf code/vehicle_struct.o code/vehicle_struct\n",
    "! ls -alh code/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
