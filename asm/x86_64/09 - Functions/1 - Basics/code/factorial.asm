        section .data
rfmt:   db "factorial(%d) = %d" , 0xa, 0

         
        segment .text
        global  main
        extern printf

fac: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        sub rsp, 16         ; with 16 bytes ( 8 bytes for the old stack pointer + 8 bytes for N)
        
        cmp rdi, 1     
        jg  facN            ; if n>1, jump to facN, otherwise continue to factorial of 1
        mov rax, 1          ; rax holds the return value - factorial of 1 = 1
        leave               ; cleanup my stack rame
        ret
facN: 
        mov [rsp+8], rdi    ; save N value in our local stack frame after the stack pointer
        dec rdi             ; remove part of the problem
        call fac            ; make the recursive call and assume it works, part-solution is now in rax
        imul rax, [rsp+8]   ; n*factorial(n-1) - multiply this n by the rest of the part solution 
        leave
        ret
        
        

main:   
        mov     r15, 12      ; hold onto the input variable N somewhere 
        
        mov     rdi, r15     ; call with 1 argument
        call    fac          ; rax now contains the full solution
        
        
        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, r15     ; parameter 2 for printf
        mov     rdx, rax     ; parameter 3 for printf
        xor     rax, rax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call