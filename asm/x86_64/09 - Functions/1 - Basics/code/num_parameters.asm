    segment .text
    global _start


_start:

    
    pop rdi        ; the first value on the stack is the number of parameters
                   ; rdi is the exit code for the following exit syscall


    
    mov rax, 0x3C ; 0x3C is the exit syscall number (don't worry we'll get to that later)
    syscall       ; execute a system call