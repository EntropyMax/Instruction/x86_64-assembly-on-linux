    segment .text
    global _start


some_fn:
    add r10, 20    ; add 20 to whatever is volatile register r10
    ret            ; return to start

_start:

    mov r10, 7    ; r10 is a volatile register
    call some_fn  ; informally increment this register!
    
    mov rax, 0x3C ; 0x3C is the exit syscall number (don't worry we'll get to that later)
    mov rdi, r10  ; mov my modified variable into the first parameter of the exit code
    syscall       ; execute a system call