        section .data
rfmt:   db "stack string is %s" , 0xa, 0

         
        segment .text
        global  main
        extern printf

func_with_local_var: 
        push rbp            ; create a new
        mov rbp, rsp        ; stack frame
        sub rsp, 256         ; with 16 bytes ( 8 bytes for the old stack pointer + 8 bytes for N)
        
        mov  byte   [rsp+8],  "H" ;
        mov  byte   [rsp+9],  "e" ;
        mov  byte   [rsp+10], "l" ;
        mov  byte   [rsp+11], "l" ;
        mov  byte   [rsp+12], "o" ;
        mov  byte   [rsp+13], " " ;
        mov  byte   [rsp+14], "W" ;
        mov  byte   [rsp+15], "o" ;
        mov  byte   [rsp+16], "r" ;
        mov  byte   [rsp+17], "l" ;
        mov  byte   [rsp+18], "d" ;
        mov  byte   [rsp+19], 0   ; // null terminator  
        
        
        lea     rdi, [rfmt]    ; parameter 1 for printf
        mov     rsi, rsp       ; parameter 2 for printf
        add     rsi, 8         ; 
        xor     rax, rax       ; 0 floating point parameters
        call    printf



        leave               ; cleanup my stack rame
        ret


main:   
        call    func_with_local_var
        
        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call