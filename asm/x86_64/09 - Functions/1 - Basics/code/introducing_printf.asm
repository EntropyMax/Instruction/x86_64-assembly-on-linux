        section .data
rfmt:   db "arguments: %d %d %d %d %d" , 0xa, 0

         
        segment .text
        global  main
        extern printf


main:   

        lea     rdi, [rfmt]  ; parameter 1 for printf
        mov     rsi, 2       ; parameter 2 for printf
        mov     rdx, 3       ; parameter 3 for printf
        mov     rcx, 4       ; parameter 4 for printf
        mov     r8,  5       ; parameter 4 for printf
        mov     r9,  6       ; parameter 4 for printf
        
        xor     rax, rax     ; 0 floating point parameters
        call    printf


        mov rax, 0x3C ; sys_exit
        mov rdi,0     ; exit(0)
        syscall       ; execute a system call