includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d * %I64d = %I64d + (%I64d << 64)", 10, 0
a dq -120
b dq 2
m dq 0
l dq 0
;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.code

main proc
    xor rax, rax
    xor rdx, rdx

    mov rax, [a]

    imul [b]
    mov [m], rdx
    mov [l], rax

    sub rsp, 56

    lea rcx, [format]
    mov rdx, [a]
    mov r8, [b]
    mov r9, [l]
    mov r10, [m]
    mov QWORD PTR [rsp+32], r10

    call printf

    add rsp, 56

    call ExitProcess

main endp
end