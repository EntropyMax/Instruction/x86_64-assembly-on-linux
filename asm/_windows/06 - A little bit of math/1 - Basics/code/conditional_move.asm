includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d - %I64d = %I64d (should display 0 if the result is less than 0)", 10, 0
a dq 20
b dq 25

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         
.code

main proc
    xor rax, rax
    xor rdx, rdx

    mov rax, [a]
    sub rax, [b]

    cmovl rax, rdx

    lea rcx, [format]    ;parameter 1, format string for printf
    mov rdx, [a]         ;parameter 2, first value
    mov r8, [b]          ;parameter 3, second value
    mov r9, rax          ;parameter 4, result of conditional move

    call printf

    call ExitProcess

main endp
end