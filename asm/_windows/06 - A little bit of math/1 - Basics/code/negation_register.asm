includelib kernel32.lib                   ;library for ExitProcess
includelib libcmt.lib                     ;library for printf
includelib libvcruntime.lib               ;library for printf
includelib libucrt.lib                    ;library for printf
includelib legacy_stdio_definitions.lib   ;library for printf

printf proto
ExitProcess proto

.data

format db "The number is: %I64d", 10, 0  ;arg 1 for printf, setting format
sample db 12                             ;value we will be calculating with

.code

main proc
    mov rax, [sample]                    
    neg rax                              ;perform negation operation on value 12 in register
    lea rcx, [format]                    ;loading address of variable format
                                         ;rcx is the register for first arg of printf
    mov rdx, rax                         ;rdx is the register for the second arg of printf
    call printf

    call ExitProcess                     ;if you do not ExitProcess and instead ret 
                                         ;an exception will be thrown and program does not exit cleanly
main endp
end



;;;;;;;;;;;;;;;;;;;;;Bare code that works;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       .code

;main   proc      


;       mov rax, 200  ; loading 200
;       neg rax       ; expect -200

;       ret
        
        
;main   endp
;       end





