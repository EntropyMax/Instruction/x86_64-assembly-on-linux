includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d - %I64d = %I64d", 10, 0
a dq 151
b dq 310

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.code

main proc
    xor rax, rax
    mov rax, [a]
    sub rax, [b]
    lea rcx, [format]    ;first parameter for printf
    mov rdx, [a]         ;second parameter for printf
    mov r8, [b]          ;third parameter for printf
    mov r9, rax          ;fourth parameter for printf
    call printf

    call ExitProcess
main endp
end