includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d + %I64d = %I64d", 10, 0
a dq 151
b dq 310

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.code

main proc
    xor rax, rax 
    add rax, [a]      
    add rax, [b]
    lea rcx, [format]    ;rcx is the first register for printf, the format of the string
    mov rdx, [a]         ;rdx is the second register for printf, the value of the first %I64d
    mov r8, [b]          ;r8 is the third register for printf, the value for the second %I64d 
    mov r9, rax          ;r9 is the fourth register for printf, the value for the third %I64d 
    call printf

    call ExitProcess
main endp
end