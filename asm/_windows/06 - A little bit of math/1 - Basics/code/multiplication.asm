includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d * %I64d = %I64d + (%I64d << 64)", 10, 0
a dq 120
b dq 2
m dq 0
l dq 0

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.code

main proc
    xor rax, rax
    xor rdx, rdx
    mov rax, [a]
    mul [b]
    mov [m], rdx
    mov [l], rax

    sub rsp, 56                    ;sub from stack pointer because of 5 parameters
	
    lea rcx, [format]              ;parameter 1, format for string
    mov rdx, [a]                   ;parameter 2, first variable
    mov r8, [b]                    ;parameter 3, second variable
    mov r9, [l]                    ;parameter 4, product of two variables
    mov r10, [m]                   ;overflow of product between two variables moved to register, since variable can't be loaded into pointer
    mov QWORD PTR [rsp+32], r10    ;parameter 5
    call printf

    add rsp, 56                    

    call ExitProcess
main endp
end