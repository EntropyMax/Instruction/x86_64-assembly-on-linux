includelib kernel32.lib
includelib libcmt.lib
includelib libvcruntime.lib
includelib libucrt.lib
includelib legacy_stdio_definitions.lib

printf	proto
ExitProcess	proto
.data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  Exercise Variables   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format db "The result is: %I64d / %I64d = quotient %I64d and remainder %I64d", 10, 0
a dq 10
b dq 3
q dq 0
r dq 0

;;;;  End Exercise Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.code

main proc
    xor rax, rax
    xor rdx, rdx
    mov rax, [a]
    div [b]
    mov [r], rdx
    mov [q], rax

    sub rsp, 56

	
    lea rcx, [format]               ;parameter 1 for printf, format of string
    mov rdx, [a]                    ;parameter 2, value to be divided
    mov r8, [b]                     ;parameter 3, value to be divided by
    mov r9, [q]                     ;parameter 4, quotient
    mov r10, [r]                    ;move remainder value into register since pointer can't take variables
    mov QWORD PTR [rsp+32], r10     ;parameter 5, remainder
    call printf

    add rsp, 56

    call ExitProcess
main endp
end