                includelib kernel32.lib
ExitProcess     proto
                .code
main            proc             ; callable executable
                mov rcx, 5       ; exit code 5
                call ExitProcess ; should be found in kernel32.lib
main            endp
                end
