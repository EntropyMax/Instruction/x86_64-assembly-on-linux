from os import environ, name
from sys import exit

if name == "posix":
    config = "/home/jovyan/.jupyter/jupyter_notebook_config.py"
elif name == "nt":
    config = "c:/conf/jupyter_notebook_config.py"
else:
    exit(101)


with open(config, "r") as f:
    data = f.read()

base_url = environ.get("BASE_URL", "/")

new_data = data\
    .replace("# c.NotebookApp.base_url = '/'", f"c.NotebookApp.base_url = '{base_url}'")\
    .replace("# c.NotebookApp.allow_origin = ''", "c.NotebookApp.allow_origin = '*'")\
    .replace("# c.NotebookApp.allow_remote_access = False", "c.NotebookApp.allow_remote_access = True")\
    .replace("# c.JupyterApp.answer_yes = False", "c.JupyterApp.answer_yes = True")\
    .replace("# c.NotebookApp.port_retries = 50", "c.NotebookApp.port_retries = 0")\
    .replace("# c.NotebookApp.quit_button = True", "c.NotebookApp.quit_button = False")\
    .replace("# c.NotebookApp.terminals_enabled = True", "c.NotebookApp.terminals_enabled = False")\
    .replace("# c.NotebookApp.trust_xheaders = False", "c.NotebookApp.trust_xheaders = True")\
    .replace("# c.Application.log_level = 30", "c.Application.log_level = 10") \
    .replace("# c.NotebookApp.allow_remote_access = False", "c.NotebookApp.allow_remote_access = True")

#.replace("#c.NotebookApp.certfile = ''", "c.NotebookApp.certfile = '/opt/jupyter/certs/server.pem'") \
#.replace("#c.NotebookApp.keyfile = ''", "c.NotebookApp.keyfile = '/opt/jupyter/certs/server.key'") \
# this is when the reverse proxy handles the authentication!
#new_data = new_data.replace("#c.NotebookApp.token = '<generated>'", f"c.NotebookApp.token = ''")

with open(config+".bak", "w") as f:
    f.write(data)
with open(config, "w") as f:
    f.write(new_data)

