# x86_64-Assembly-on-Linux




Linux Assembly Startup
------------------------


`TAG_NUMBER` below is a variable that can be obtained by checking this container registry for the most recent build



```bash

    docker run -d --rm --name training-notebook  -p8888:8888 registry.gitlab.com/90cos/cyt/training/modules/x86_64-assembly-on-linux:$TAG_NUMBER

```

This will start the notebook as a background process and forward port 8888/tcp on the host into the container.

Select any host port to redirect to the container 8888

Multiple people can use the same server with multiple redirect ports

example that redirects host port 7777 to 

```bash

    docker run -d --rm --name training-notebook-another-one  -p7777:8888

```


Access the training from any web browser with network to the server this container image is running


The first example, running the container on a development machine would navigate to `http://localhost:8888`


In a terminal, perform `docker logs training-notebook` to obtain the access token to access the notebook.

Put that token in the login screen of the web browser.


Note: all data is stored within the container at this time.  If the container is shut down, the host computer rebooted, or crashes, all work done within the container will be lost.

Suggest File - Save As - .ipynb to download completed notebooks to your hosts computer.
